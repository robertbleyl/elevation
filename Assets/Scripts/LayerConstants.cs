public class LayerConstants {

    public static string Objects = "Objects";
    public static string Walls = "Walls";
    public static string StaticInterior = "StaticInterior";
    public static string Characters = "Characters";
    public static string HeavyObjects = "HeavyObjects";
    public static string CaughtObjects = "CaughtObjects";
    public static string PullingInObjects = "PullingInObjects";
    public static string PickedUpObjects = "PickedUpObjects";
    public static string ThrownObjects = "ThrownObjects";
}
