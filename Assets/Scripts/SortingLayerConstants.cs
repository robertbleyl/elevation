public class SortingLayerConstants {

    public static string Objects = "Objects";
    public static string GunOnGround = "GunOnGround";
    public static string CaughtObjects = "CaughtObjects";
    public static string EquippedObjects = "EquippedObjects";
}
