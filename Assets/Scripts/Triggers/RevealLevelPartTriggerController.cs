using System.Collections;
using UnityEngine;

public class RevealLevelPartTriggerController : MonoBehaviour {

    [SerializeField]
    private float revealTime = 1f;
    [SerializeField]
    private SpriteRenderer spriteRenderer = null;
    [SerializeField]
    private GameObject content = null;

    private float revealTimer = -100f;

    private void Start () {
        if (content != null) {
            StartCoroutine (deactivateContent ());
        }
    }

    private IEnumerator deactivateContent () {
        yield return new WaitForSecondsRealtime (1f);
        content.SetActive (false);
    }

    private void Update () {
        if (revealTimer > 0f) {
            if (content != null) {
                content.SetActive (true);
            }

            revealTimer -= Time.deltaTime;

            float alpha = revealTimer / revealTime;
            spriteRenderer.color = new Color (spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, alpha);

            if (revealTimer <= 0f) {
                Destroy (this);
            }
        }
    }

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null && revealTimer < 0f) {
            revealTimer = revealTime;
        }
    }
}
