using UnityEngine;

public class ObjectiveTextChangeTriggerController : MonoBehaviour {

    [SerializeField]
    private string text;

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null) {
            Events.instance.objectiveChanged (text);
            Destroy (this);
        }
    }
}
