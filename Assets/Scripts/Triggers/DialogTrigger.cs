using UnityEngine;
using static ScreensManager;

public class DialogTrigger : MonoBehaviour {

    [SerializeField]
    private Dialog dialog = null;

    private DialogScreenController dialogScreenController;

    private void Start () {
        dialogScreenController = GameObject.FindGameObjectWithTag (Tags.DialogScreen).GetComponent<DialogScreenController> ();
    }

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null) {
            dialogScreenController.setActiveDialog (dialog);
            Events.instance.changeScreen (ScreenType.DIALOG);
            gameObject.SetActive (false);
        }
    }
}
