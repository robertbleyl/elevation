using UnityEngine;

public class DemoTutorialInfoTriggerController : MonoBehaviour {

    [SerializeField]
    private GameObject toBeRevealed = null;

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null) {
            toBeRevealed.SetActive (true);
            Destroy (this);
        }
    }
}