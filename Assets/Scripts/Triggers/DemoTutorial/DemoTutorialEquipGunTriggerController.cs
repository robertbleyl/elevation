using UnityEngine;

public class DemoTutorialEquipGunTriggerController : MonoBehaviour {

    [SerializeField]
    private string objecitveText;
    [SerializeField]
    private GameObject[] toBeRevealed;

    private void Start () {
        Events.instance.onEquipGun += onEquipGun;
    }

    private void OnDestroy () {
        Events.instance.onEquipGun -= onEquipGun;
    }

    private void onEquipGun (EquipGunEvent equipGunEvent) {
        Events.instance.objectiveChanged (objecitveText);

        foreach (GameObject obj in toBeRevealed) {
            obj.SetActive (true);
        }

        Destroy (this);
    }
}
