using UnityEngine;

public class DemoTutorialKillEnemyTriggerController : MonoBehaviour {

    [SerializeField]
    private GameObject enemy = null;
    [SerializeField]
    private string objectiveText;

    private void Start () {
        Events.instance.onCharacterDied += onCharacterDied;
    }

    private void onCharacterDied (CharacterDeathEvent obj) {
        if (obj.character == enemy) {
            Events.instance.onCharacterDied -= onCharacterDied;

            Events.instance.objectiveChanged (objectiveText);
            Destroy (this);
        }
    }
}
