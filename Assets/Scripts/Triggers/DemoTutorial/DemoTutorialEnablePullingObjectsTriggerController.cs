using UnityEngine;

public class DemoTutorialEnablePullingObjectsTriggerController : MonoBehaviour {

    private Progression progression;

    private void Start () {
        progression = GameObject.FindGameObjectWithTag (Tags.Player).GetComponent<Progression> ();
    }

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null) {
            progression.pullingObjectsEnabled = true;
            Destroy (this);
        }
    }
}
