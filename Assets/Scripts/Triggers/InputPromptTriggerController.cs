using UnityEngine;

public class InputPromptTriggerController : MonoBehaviour {

    [SerializeField]
    private string key;
    [SerializeField]
    private string label;
    [SerializeField]
    private Vector2 offset;

    private bool displaying;

    protected virtual void OnTriggerEnter2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null) {
            Events.instance.displayInputPrompt (new DisplayInputPromptEvent (transform.parent.gameObject, key, label, offset));
            displaying = true;
        }
    }

    protected virtual void OnTriggerExit2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null && displaying) {
            Events.instance.hideInputPrompt ();
            displaying = false;
        }
    }
}
