using UnityEngine;

public class EquipGunPromptTriggerController : InputPromptTriggerController {

    [SerializeField]
    private GunController gunController = null;

    private PlayerController playerController;

    private void Start () {
        playerController = GameObject.FindGameObjectWithTag (Tags.Player).GetComponent<PlayerController> ();
    }

    protected override void OnTriggerEnter2D (Collider2D collider) {
        if (playerController?.equipedGun == null || playerController?.equipedGun.gunType != gunController.gunType) {
            base.OnTriggerEnter2D (collider);
        }
    }
}
