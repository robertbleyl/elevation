using UnityEngine;

public class EndTriggerController : MonoBehaviour {

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.GetComponent<PlayerController> () != null) {
            Events.instance.levelFinished ();
            gameObject.SetActive (false);
        }
    }
}
