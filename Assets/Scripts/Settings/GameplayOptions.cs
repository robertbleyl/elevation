using UnityEngine;

public class GameplayOptions {

    private static readonly string AUTO_THROW_KEY = "AutoThrow";
    private static readonly string AUTO_RELOAD_KEY = "AutoReload";

    public static bool autoThrowEnabled () {
        return PlayerPrefs.GetInt (AUTO_THROW_KEY) == 1;
    }

    public static bool autoReloadEnabled () {
        return PlayerPrefs.GetInt (AUTO_RELOAD_KEY) == 1;
    }

    public static void setAutoThrow (bool enabled) {
        PlayerPrefs.SetInt (AUTO_THROW_KEY, enabled ? 1 : 0);
        PlayerPrefs.Save ();
    }

    public static void setAutoReload (bool enabled) {
        PlayerPrefs.SetInt (AUTO_RELOAD_KEY, enabled ? 1 : 0);
        PlayerPrefs.Save ();
    }
}
