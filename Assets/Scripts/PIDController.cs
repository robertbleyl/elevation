using UnityEngine;

public class PIDController {

    private Vector2 lastError = new Vector2 ();
    private Vector2 derivative = new Vector2 ();
    private Vector2 integral = new Vector2 ();

    private float errorWeight;
    private float integralWeight;
    private float derivativeWeight;

    public PIDController (float errorWeight, float integralWeight, float derivativeWeight) {
        this.errorWeight = errorWeight;
        this.integralWeight = integralWeight;
        this.derivativeWeight = derivativeWeight;
    }

    public Vector2 getCorrection (Vector2 error) {
        derivative = (error - lastError) / Time.deltaTime;
        integral += error * Time.deltaTime;
        lastError = error;

        Vector2 correction = (error * errorWeight) + (integral * integralWeight) + (derivative * derivativeWeight);
        return correction;
    }
}