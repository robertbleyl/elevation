using UnityEngine;

public class DropsObjectOnDeath : MonoBehaviour {

    [SerializeField]
    [Tooltip ("Chance that Item will Drop between 0 and 100")]
    [Range (0, 100)]
    private int dropChance = 50;
    [SerializeField]
    [Tooltip ("Number of Objects that will be dropped")]
    [Min (0)]
    private int count = 4;
    [SerializeField]
    private float distributionForce = 100f;
    [SerializeField]
    private GameObject dropGameObject = null;
    [SerializeField]
    private bool triggerOnlyWhenMelee;

    private void Start () {
        Events.instance.onCharacterDied += onCharacterDied;
    }

    private void OnDestroy () {
        Events.instance.onCharacterDied -= onCharacterDied;
    }

    private void onCharacterDied (CharacterDeathEvent characterDeathEvent) {
        if (characterDeathEvent.character == gameObject && (!triggerOnlyWhenMelee || characterDeathEvent.deathByMelee) && dropGameObject != null && Random.value * 100 <= dropChance) {
            for (int i = 0; i < count; i++) {
                GameObject newDrop = Instantiate (dropGameObject);
                newDrop.transform.SetParent (transform.parent);
                newDrop.transform.position = transform.position;

                Rigidbody2D rb = newDrop.GetComponent<Rigidbody2D> ();

                if (rb != null) {
                    rb.AddForce (Random.insideUnitCircle * distributionForce);
                }
            }
        }
    }
}
