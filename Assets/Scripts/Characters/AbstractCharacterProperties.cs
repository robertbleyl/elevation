using UnityEngine;

public abstract class AbstractCharacterProperties : ScriptableObject {

    [SerializeField]
    private GameObject assaultRifleHandsPrefab;
    [SerializeField]
    private GameObject shotGunHandsPrefab;
    [SerializeField]
    private GameObject sniperRifleHandsPrefab;
    [SerializeField]
    private float movementSpeed = 3f;
    [SerializeField]
    private int initAmmo = 20;
    [SerializeField]
    private int maxAmmo = 50;
    [SerializeField]
    private float meleeRange = 1.5f;

    public GameObject getAssaultRifleHandsPrefab () {
        return assaultRifleHandsPrefab;
    }

    public GameObject getShotGunHandsPrefab () {
        return shotGunHandsPrefab;
    }

    public GameObject getSniperRifleHandsPrefab () {
        return sniperRifleHandsPrefab;
    }

    public float getMovementSpeed () {
        return movementSpeed;
    }

    public int getInitAmmo () {
        return initAmmo;
    }

    public int getMaxAmmo () {
        return maxAmmo;
    }

    public float getMeleeRange () {
        return meleeRange;
    }
}
