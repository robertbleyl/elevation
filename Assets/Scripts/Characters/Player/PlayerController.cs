using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;
using static AbstractMovementState;
using static ScreensManager;

public class PlayerController : AbstractCharacterController<PlayerProperties> {

    [SerializeField]
    private bool godMode;

    [SerializeField]
    private Collider2D collider2d = null;
    [SerializeField]
    private Rigidbody2D rigidbody2d = null;
    [SerializeField]
    private Animator animator = null;

    [SerializeField]
    public GameObject pulledObjectLocation = null;
    [SerializeField]
    private PullingObjects pullingObjects = null;
    [SerializeField]
    private NearestEnemy nearestEnemy = null;
    [SerializeField]
    private Progression progression = null;
    [SerializeField]
    private float cameraMouseFactorMax = 0.65f;
    [SerializeField]
    private SpriteRenderer spriteRenderer = null;

    public ObjectController pulledObject;
    public bool pulledObjectIsGun;
    public float currentShields;
    public float currentEnergy;

    private InputMain controls;

    private LayerMask objectsLayer;
    private CinemachineVirtualCamera virtualCamera;
    private CinemachineFramingTransposer transposer;

    private PIDController pidController = new PIDController (10f, 0f, 0.05f);

    private GunController currentNearGunOnGround;

    private Vector2 moveDirection;
    private bool shootGun;

    private float gunEmptySoundPlayTimer;
    private float noPullEnergySoundPlayTimer;

    private float originalDrag;
    private float meleeAttackTimer;
    private Vector3 meleeAttackDirection;

    private float energyRecoveryDelayTimer;

    private void Awake () {
        controls = new InputMain ();
        controls.Player.Shoot.performed += ctx => startShooting ();
        controls.Player.Shoot.canceled += ctx => stopShooting ();
        controls.Player.PullAndThrowObject.performed += ctx => pullObject ();
        controls.Player.PullAndThrowObject.canceled += ctx => throwObject ();
        controls.Player.PickupOrReplaceGun.performed += ctx => pickupAndReplaceGun ();
        controls.Player.ReloadGun.performed += ctx => reloadGun ();
        controls.Player.Melee.performed += ctx => performMeleeAttack ();
        controls.Player.Movement.performed += ctx => setMoveDirection (ctx.ReadValue<Vector2> ());
        controls.Player.Movement.canceled += ctx => setMoveDirection (Vector2.zero);
    }

    protected override void Start () {
        base.Start ();

        originalDrag = rigidbody2d.drag;

        objectsLayer = LayerMask.GetMask (LayerConstants.Objects);
        virtualCamera = GameObject.FindGameObjectWithTag (Tags.VirtualCamera).GetComponent<CinemachineVirtualCamera> ();
        transposer = virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer> ();

        currentEnergy = properties.getMaxEnergy ();

        Events.instance.onCharacterDied += onCharacterDied;
        Events.instance.onChangeScreen += onChangeScreen;
        Events.instance.onLevelFinished += onLevelFinished;
    }

    private void onLevelFinished () {
        enabled = false;
    }

    private void onCharacterDied (CharacterDeathEvent characterDeathEvent) {
        if (characterDeathEvent.isPlayer) {
            enabled = false;
        }
    }

    private void onChangeScreen (ScreenType screenType) {
        enabled = screenType == ScreenType.HUD;
    }

    private void startShooting () {
        shootGun = true;
    }

    private void stopShooting () {
        shootGun = false;
    }

    private void setMoveDirection (Vector2 moveDirection) {
        this.moveDirection = moveDirection;
        animator.SetFloat ("Speed", moveDirection.sqrMagnitude);
    }

    private void pullObject () {
        if (!progression.pullingObjectsEnabled) {
            return;
        }

        if (pulledObject != null && pulledObject.transform.parent == pulledObjectLocation.transform) {
            return;
        }

        if (pullingObjects.currentNearest == null) {
            return;
        }

        if (currentEnergy < pullingObjects.currentNearest.properties.getRequiredEnergyForPull ()) {
            checkNoPullEnergySound ();
            return;
        }

        pulledObject = pullingObjects.currentNearest;
        currentEnergy -= pulledObject.properties.getRequiredEnergyForPull ();
        energyRecoveryDelayTimer = properties.getEnergyRecoveryDelay ();

        Physics2D.IgnoreCollision (pulledObject.GetComponent<Collider2D> (), collider2d);

        pulledObjectIsGun = pulledObject.GetComponent<GunController> () != null;
        pulledObject.changeMovementState (MovementStateType.PICKING_UP);
    }

    private void checkNoPullEnergySound () {
        if (noPullEnergySoundPlayTimer <= 0f) {
            FMODUnity.RuntimeManager.PlayOneShot (properties.getNoPullEnergySound (), transform.position);
            noPullEnergySoundPlayTimer = properties.getNoPullEnergySoundDuration ();
        }
    }

    private void throwObject () {
        if (enabled && progression.pullingObjectsEnabled && pulledObject != null) {
            MovementStateType stateType = pulledObject.getActiveMovementStateType ();

            if (stateType == MovementStateType.PICKING_UP) {
                setupNextThrowDirection ();
                pulledObject = null;
            } else if (stateType == MovementStateType.CAUGHT || stateType == MovementStateType.PULLING) {
                setupNextThrowDirection ();
                pulledObject.changeMovementState (MovementStateType.THROW);
            }
        }
    }


    private void setupNextThrowDirection () {
        Vector2 nextThrowDirection;

        if (GameplayOptions.autoThrowEnabled () && nearestEnemy.currentNearest != null) {
            nextThrowDirection = (Vector2)nearestEnemy.currentNearest.transform.position - pulledObject.rigidBody.position;
        } else {
            Vector2 mouseWorldPos = Camera.main.ScreenToWorldPoint (Mouse.current.position.ReadValue ());
            nextThrowDirection = mouseWorldPos - pulledObject.rigidBody.position;
        }

        pulledObject.setupNextThrowDirection (nextThrowDirection);
    }

    private void pickupAndReplaceGun () {
        if (currentNearGunOnGround == null) {
            return;
        }

        dropGunIfEquiped ();

        equipGun (currentNearGunOnGround);

        currentNearGunOnGround = null;

        Events.instance.equipGun (new EquipGunEvent (gameObject, true));
    }

    private void reloadGun () {
        if (equipedGun == null || equipedGun.reloading) {
            return;
        }

        if (currentAmmo == 0) {
            // TODO play sound
            return;
        }

        int toBeReloaded = equipedGun.ammoCapacity - equipedGun.getAvailableAmmo ();

        if (toBeReloaded == 0) {
            return;
        }

        toBeReloaded = Mathf.Min (toBeReloaded, currentAmmo);

        if (toBeReloaded == 0) {
            return;
        }

        equipedGun.reload (equipedGun.getAvailableAmmo () + toBeReloaded);

        currentAmmo -= toBeReloaded;
    }

    private void performMeleeAttack () {
        if (meleeAttackTimer > 0f) {
            return;
        }

        if (nearestEnemy.currentNearest == null || nearestEnemy.currentNearestDist > properties.getMeleeAttackRange ()) {
            return;
        }

        EnemyController enemyController = nearestEnemy.currentNearest.GetComponent<EnemyController> ();

        if (!(godMode || enemyController.stunned) || enemyController.currentArmor > 0f) {
            return;
        }

        nearestEnemy.currentNearest.GetComponent<EnemyHealthController> ().killByMelee ();

        Physics2D.IgnoreCollision (collider2d, nearestEnemy.currentNearest.GetComponent<Collider2D> ());
        meleeAttackDirection = nearestEnemy.currentNearest.transform.position - transform.position;

        rigidbody2d.drag = properties.getMeleeAttackDrag ();
        meleeAttackTimer = properties.getMeleeAttackDuration ();
    }

    private void FixedUpdate () {
        if (meleeAttackTimer > 0f) {
            meleeAttackTimer -= Time.deltaTime;

            Vector2 targetSpeed = meleeAttackDirection * properties.getMeleeAttackTargetSpeed ();
            Vector2 error = targetSpeed - rigidbody2d.velocity;
            Vector2 force = pidController.getCorrection (error);

            rigidbody2d.AddForce (force);
        } else if (rigidbody2d.velocity.magnitude < properties.getMeleeAttackRestoreVelocity ()) {
            rigidbody2d.drag = originalDrag;
        }

        if (moveDirection != Vector2.zero) {
            rigidbody2d.MovePosition (rigidbody2d.position + moveDirection * properties.getMovementSpeed () * Time.fixedDeltaTime);
        }
    }

    private void Update () {
        Vector3 mousePos = getMousePos ();

        rotateGunToTarget (mousePos);

        Vector3 mousePosInSpace = Camera.main.ScreenToWorldPoint (mousePos);
        Vector3 mouseDir = mousePosInSpace - transform.position;
        pulledObjectLocation.transform.position = transform.position + (mouseDir.normalized * properties.getPulledObjectDistance ());

        looksLeft = mouseDir.x <= 0f;
        spriteRenderer.flipX = !looksLeft;

        if (equipedGun != null) {
            if (equipedGun.getAvailableAmmo () == 0 && GameplayOptions.autoReloadEnabled ()) {
                reloadGun ();
            } else if (shootGun && !equipedGun.reloading) {
                shoot (mouseDir, true);
                checkGunEmptySound ();
            }
        }

        if (noPullEnergySoundPlayTimer > 0f) {
            noPullEnergySoundPlayTimer -= Time.deltaTime;
        }

        updateCameraOffset (mousePosInSpace);

        recoverEnergy ();
    }

    private void recoverEnergy () {
        if (energyRecoveryDelayTimer > 0f) {
            energyRecoveryDelayTimer -= Time.deltaTime;
        } else {
            currentEnergy += Time.deltaTime * properties.getEnergyRecoveryPerSecond ();

            if (currentEnergy > properties.getMaxEnergy ()) {
                currentEnergy = properties.getMaxEnergy ();
            }
        }
    }

    private Vector3 getMousePos () {
        Vector2 mousePos2D = Mouse.current.position.ReadValue ();
        return new Vector3 (mousePos2D.x, mousePos2D.y);
    }

    public override void equipGun (GunController gun, bool silent) {
        base.equipGun (gun, silent);
        animator.SetBool ("HasGun", true);
    }

    private void OnTriggerEnter2D (Collider2D collider) {
        GunController gun = collider.GetComponent<GunController> ();

        if (gun == null || gun == equipedGun) {
            return;
        }

        if (equipedGun != null) {
            addAmmoFromGun (gun);
        } else if (gun != currentNearGunOnGround && (currentNearGunOnGround == null
        || Vector2.Distance (transform.position, gun.transform.position) < Vector2.Distance (transform.position, currentNearGunOnGround.transform.position))) {
            currentNearGunOnGround = gun;
        }
    }

    public void addAmmoFromGun (GunController gun) {
        int ammoToBeExtracted = Mathf.Min (gun.getAvailableAmmo (), properties.getMaxAmmo () - currentAmmo);

        gun.setAmmo (gun.getAvailableAmmo () - ammoToBeExtracted);

        addAmmo (ammoToBeExtracted);
    }

    public void addAmmoFromDrop (AmmoDropController ammoDrop) {
        addAmmo (ammoDrop.ammoAmount);
    }

    private void checkGunEmptySound () {
        if (equipedGun.isEmpty ()) {
            if (gunEmptySoundPlayTimer <= 0f) {
                FMODUnity.RuntimeManager.PlayOneShot (properties.getGunEmptySound (), transform.position);
                gunEmptySoundPlayTimer = properties.getGunEmptySoundDuration ();
            } else {
                gunEmptySoundPlayTimer -= Time.deltaTime;
            }
        }
    }

    private void updateCameraOffset (Vector3 mousePosInSpace) {
        float maxX = Screen.width * cameraMouseFactorMax;
        float maxY = Screen.height * cameraMouseFactorMax;

        float offsetX = getOffsetCoordinate (maxX, mousePosInSpace.x, transform.position.x);
        float offsetY = getOffsetCoordinate (maxY, mousePosInSpace.y, transform.position.y);
        transposer.m_TrackedObjectOffset = new Vector3 (offsetX, offsetY);
    }

    private float getOffsetCoordinate (float max, float mouseCoord, float playerCoord) {
        float diff = Mathf.Min (max, mouseCoord - playerCoord);
        return diff / 5f;
    }

    public float getAmmoPercentage () {
        return currentAmmo / (float)properties.getMaxAmmo ();
    }

    public float getEnergyPercentage () {
        return currentEnergy / (float)properties.getMaxEnergy ();
    }

    private void OnEnable () {
        controls.Enable ();
    }

    private void OnDisable () {
        controls.Disable ();
    }

    public override bool isDead () {
        return currentShields <= 0f;
    }
}
