using UnityEngine;

public class NearestEnemy : MonoBehaviour {

    [SerializeField]
    private float maxDist = 6f;

    private GameObject player;

    private bool newFrame = true;
    public float currentNearestDist = float.MaxValue;

    public GameObject currentNearest;

    private void Start () {
        player = GameObject.FindGameObjectWithTag (Tags.Player);
    }

    private void onCharacterDied (CharacterDeathEvent characterDeathEvent) {
        if (characterDeathEvent.isPlayer) {
            enabled = false;

            if (currentNearest != null) {
                currentNearest = null;
            }
        }
    }

    public void checkEnemy (GameObject enemy) {
        if (!enabled) {
            return;
        }

        if (newFrame) {
            currentNearestDist = float.MaxValue;

            if (currentNearest != null) {
                currentNearest = null;
            }
            newFrame = false;
        }

        float dist = Vector2.Distance (enemy.transform.position, player.transform.position);

        if (dist < currentNearestDist && dist <= maxDist) {
            currentNearest = enemy;
            currentNearestDist = dist;
        }
    }

    private void LateUpdate () {
        newFrame = true;
    }
}
