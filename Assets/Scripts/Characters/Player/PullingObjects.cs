using UnityEngine;
using UnityEngine.InputSystem;

public class PullingObjects : MonoBehaviour {

    [SerializeField]
    private float maxDist = 3f;

    private Progression progression;

    private bool newFrame = true;
    private float currentNearestDist = float.MaxValue;
    private Vector2 currentMousePos;

    public ObjectController currentNearest;

    private void Start () {
        progression = GameObject.FindGameObjectWithTag (Tags.Player).GetComponent<Progression> ();
    }

    private void onCharacterDied (CharacterDeathEvent characterDeathEvent) {
        if (characterDeathEvent.isPlayer) {
            enabled = false;

            if (currentNearest != null) {
                currentNearest.hideHightlight ();
                currentNearest = null;
            }
        }
    }

    public void checkObject (ObjectController objectController) {
        if (!enabled || !progression.pullingObjectsEnabled) {
            return;
        }

        if (newFrame) {
            if (currentNearest != null) {
                currentNearest.hideHightlight ();
                currentNearest = null;
            }
            newFrame = false;
        }

        float dist = Vector2.Distance (objectController.transform.position, currentMousePos);

        if (dist < currentNearestDist && dist <= maxDist) {
            currentNearest = objectController;
            currentNearestDist = dist;
        }
    }

    private void LateUpdate () {
        if (!progression.pullingObjectsEnabled) {
            return;
        }

        currentMousePos = Camera.main.ScreenToWorldPoint (Mouse.current.position.ReadValue ());

        if (currentNearest != null && notInPossessionByAnyCharacter () && currentNearest.getRigidbody ().velocity.magnitude < 0.1f) {
            currentNearest.showHightlight ();
        }

        currentNearestDist = float.MaxValue;

        newFrame = true;
    }

    private bool notInPossessionByAnyCharacter () {
        return currentNearest.GetComponentInParent<AbstractCharacterHealthController<AbstractCharacterProperties, AbstractCharacterController<AbstractCharacterProperties>>> () == null;
    }
}
