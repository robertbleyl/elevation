using UnityEngine;

public class PlayerHealthController : AbstractCharacterHealthController<PlayerProperties, PlayerController> {

    protected override void Start () {
        base.Start ();

        controller.currentShields = 0f;

        regainShield (properties.getMaxShields ());
    }

    public void regainShield (float amount) {
        controller.currentShields += amount;
    }

    protected void OnCollisionEnter2D (Collision2D collision) {
        if (controller.isDead ()) {
            return;
        }

        BulletController bulletController = collision.collider.GetComponent<BulletController> ();

        if (bulletController != null) {
            receiveDamage (bulletController.getDamage ());
        }
    }

    public void receiveDamage (float damage) {
        controller.currentShields -= damage;

        if (controller.currentShields < 0f) {
            controller.currentShields = 0f;

            if (controller.equipedGun != null) {
                controller.dropGunIfEquiped ();
            }

            Events.instance.characterDied (new CharacterDeathEvent (gameObject, false, true));

            animator.SetBool ("Dead", true);
            rigidBody2d.simulated = false;
            collider2d.enabled = false;

            if (!deathSound.IsNull) {
                FMODUnity.RuntimeManager.PlayOneShot (deathSound, transform.position);
            }
        }
    }

    public float getCurrentShieldPercentage () {
        return controller.currentShields / properties.getMaxShields ();
    }
}
