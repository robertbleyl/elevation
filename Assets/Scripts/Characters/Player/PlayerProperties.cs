using FMODUnity;
using UnityEngine;

[CreateAssetMenu (fileName = "PlayerProperties", menuName = "PlayerProperties")]
public class PlayerProperties : AbstractCharacterProperties {

    [SerializeField]
    private float maxShields = 20f;

    [SerializeField]
    private float pullSpeed = 4f;
    [SerializeField]
    private float throwForce = 8f;
    [SerializeField]
    private float pulledObjectDistance = 2f;

    [SerializeField]
    private float maxEnergy = 100f;
    [SerializeField]
    private float energyRecoveryDelay = 1.5f;
    [SerializeField]
    private float energyRecoveryPerSecond = 4f;

    [SerializeField]
    private float meleeAttackRange = 3f;
    [SerializeField]
    private float meleeAttackTargetSpeed = 10f;
    [SerializeField]
    private float meleeAttackDuration = 1f;
    [SerializeField]
    private float meleeAttackDrag = 0.5f;
    [SerializeField]
    private float meleeAttackRestoreVelocity = 3f;

    [SerializeField]
    private EventReference gunEmptySound;
    [SerializeField]
    private float gunEmptySoundDuration = 0.31f;

    [SerializeField]
    private EventReference noPullEnergySound;
    [SerializeField]
    private float noPullEnergySoundDuration = 0.35f;

    public float getMaxShields () {
        return maxShields;
    }

    public float getPullSpeed () {
        return pullSpeed;
    }

    public float getThrowForce () {
        return throwForce;
    }

    public float getPulledObjectDistance () {
        return pulledObjectDistance;
    }

    public float getMaxEnergy () {
        return maxEnergy;
    }

    public float getEnergyRecoveryDelay () {
        return energyRecoveryDelay;
    }

    public float getEnergyRecoveryPerSecond () {
        return energyRecoveryPerSecond;
    }

    public float getMeleeAttackRange () {
        return meleeAttackRange;
    }

    public float getMeleeAttackTargetSpeed () {
        return meleeAttackTargetSpeed;
    }

    public float getMeleeAttackDuration () {
        return meleeAttackDuration;
    }

    public float getMeleeAttackDrag () {
        return meleeAttackDrag;
    }

    public float getMeleeAttackRestoreVelocity () {
        return meleeAttackRestoreVelocity;
    }

    public EventReference getGunEmptySound () {
        return gunEmptySound;
    }

    public float getGunEmptySoundDuration () {
        return gunEmptySoundDuration;
    }

    public EventReference getNoPullEnergySound () {
        return noPullEnergySound;
    }

    public float getNoPullEnergySoundDuration () {
        return noPullEnergySoundDuration;
    }
}
