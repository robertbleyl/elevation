using FMODUnity;
using UnityEngine;

public abstract class AbstractCharacterHealthController<T, E> : MonoBehaviour where T : AbstractCharacterProperties where E : AbstractCharacterController<T> {

    protected static System.Random rand = new System.Random ();

    [SerializeField]
    protected E controller = null;
    [SerializeField]
    protected Animator animator = null;
    [SerializeField]
    protected Rigidbody2D rigidBody2d = null;
    [SerializeField]
    protected Collider2D collider2d = null;

    [SerializeField]
    protected float deathTime = 2.5f;
    [SerializeField]
    protected EventReference deathSound;

    protected T properties;

    protected virtual void Start () {
        properties = controller.properties;
    }
}
