using System.Collections;
using UnityEngine;
using static AbstractMovementState;

public abstract class AbstractCharacterController<T> : MonoBehaviour where T : AbstractCharacterProperties {

    [SerializeField]
    public T properties = null;
    [SerializeField]
    protected GameObject preEquipedGun = null;
    [SerializeField]
    public GameObject gunEquipLocation = null;

    public GunController equipedGun;
    public bool looksLeft;
    public int currentAmmo;

    public abstract bool isDead ();

    protected virtual void Start () {
        currentAmmo = properties.getInitAmmo ();

        if (preEquipedGun != null) {
            GunController gun = Instantiate (preEquipedGun).GetComponent<GunController> ();
            equipedGun = gun;
            StartCoroutine (delayedEquipGun (gun));
        }
    }

    private IEnumerator delayedEquipGun (GunController gun) {
        yield return new WaitForSecondsRealtime (0.1f);
        equipGun (gun, true);
    }

    public virtual void equipGun (GunController gun) {
        equipGun (gun, false);
    }

    public virtual void equipGun (GunController gun, bool silent) {
        equipedGun = gun;

        GameObject handsPrefab = null;

        switch (equipedGun.gunType) {
            case GunType.MASHINE_GUN:
                handsPrefab = properties.getAssaultRifleHandsPrefab ();
                break;
        }

        equipedGun.equip (gunEquipLocation, handsPrefab, silent);
    }

    public virtual void dropGunIfEquiped () {
        if (equipedGun == null) {
            return;
        }

        equipedGun.GetComponent<ObjectController> ().changeMovementState (MovementStateType.NEUTRAL);
        equipedGun.transform.SetParent (transform.parent);
        equipedGun.detachHandsSprite ();
        equipedGun = null;
    }

    public void addAmmo (int amount) {
        currentAmmo += amount;

        if (currentAmmo > properties.getMaxAmmo ()) {
            currentAmmo = properties.getMaxAmmo ();
        }
    }

    public void shoot (Vector2 direction, bool player) {
        if (equipedGun != null) {
            equipedGun.fireGun (direction, player);
        }
    }

    public void rotateGunToTarget (Vector3 targetLocation) {
        if (equipedGun != null) {
            Transform gunTransform = equipedGun.transform;
            Vector3 dir = Camera.main.WorldToScreenPoint (gunTransform.position) - targetLocation;
            float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;

            gunTransform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

            float scaleY = 1f;

            if (angle <= -90f || angle > 90f) {
                scaleY = -1f;
            }

            gunTransform.localScale = new Vector3 (1f, scaleY, 1f);
        }
    }
}
