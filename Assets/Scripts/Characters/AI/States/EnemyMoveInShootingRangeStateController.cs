using UnityEngine;

public class EnemyMoveInShootingRangeStateController : AbstractEnemyPathingStateController {

    private Vector3 lastKnownLocationOfPickupGun;
    private GunController nearestGunForPickup;

    private bool hasRetreatComponent;

    protected override void Awake () {
        base.Awake ();

        hasRetreatComponent = GetComponent<EnemyRetreatFromPlayerStateController> () != null;
    }

    public override EnemyStateType getStateType () {
        return EnemyStateType.MOVE_IN_SHOOTING_RANGE;
    }

    public override EnemyStateType checkForNextState () {
        if (controller.equipedGun == null) {
            return EnemyStateType.SEARCH_FOR_GUN;
        }

        float distanceToPlayer = Vector3.Distance (transform.position, getLookTarget ());

        if (!hasRetreatComponent && distanceToPlayer > properties.getMaxEngageRange ()) {
            return EnemyStateType.IDLE;
        }

        if (controller.equipedGun != null && !pathNotReady () && distanceToPlayer <= properties.getStartingShootRange ()) {
            return EnemyStateType.SHOOT_PLAYER;
        }

        return EnemyStateType.MOVE_IN_SHOOTING_RANGE;
    }

    protected override void transitionTo () {

    }

    protected override bool shouldStartNewPath () {
        return controller.player != null;
    }

    protected override Vector3 getNextTargetPosition () {
        if (controller == null || controller.player == null) {
            return transform.position;
        }

        return controller.player.transform.position;
    }

    public override Vector3 getLookTarget () {
        if (controller == null || controller.player == null) {
            return Vector3.zero;
        }

        return controller.player.transform.position;
    }
}
