using UnityEngine;

public class EnemyRetreatFromPlayerStateController : AbstractEnemyPathingStateController {

    [SerializeField]
    private float retreatTime = 4f;
    [SerializeField]
    private EnemyStateType retreatTimeTransition = EnemyStateType.MOVE_IN_SHOOTING_RANGE;

    private float retreatTimer;

    public override EnemyStateType getStateType () {
        return EnemyStateType.RETREAT_FROM_PLAYER;
    }

    public override EnemyStateType checkForNextState () {
        if (controller.equipedGun == null) {
            return EnemyStateType.SEARCH_FOR_GUN;
        }

        if (retreatTime > 0f && retreatTimer <= 0f) {
            return retreatTimeTransition;
        }

        return EnemyStateType.RETREAT_FROM_PLAYER;
    }

    public override Vector3 getLookTarget () {
        if (controller == null || controller.player == null) {
            return Vector3.zero;
        }

        return transform.position + (transform.position - controller.player.transform.position);
    }

    protected override void transitionTo () {
        retreatTimer = retreatTime;
    }

    protected override bool shouldStartNewPath () {
        return true;
    }

    protected override Vector3 getNextTargetPosition () {
        if (controller == null || controller.player == null) {
            return transform.position;
        }

        return getLookTarget ();
    }

    private void Update () {
        if (retreatTimer > 0f) {
            retreatTimer -= Time.deltaTime;
        }
    }
}
