using FMODUnity;
using UnityEngine;

public class EnemyIdleStateController : AbstractEnemyStateController {

    [SerializeField]
    private float aggroTriggerSoundPitch = 1f;
    [SerializeField]
    private float aggroTriggerDelay = 1.3f;
    [SerializeField]
    private EventReference aggroTriggerSound;
    [SerializeField]
    private float maxRaycastCheckRange = 15f;
    [SerializeField]
    private float raycastDirectionFactor = 1.5f;
    [SerializeField]
    private EnemyStateType aggressiveStateType = EnemyStateType.MOVE_IN_SHOOTING_RANGE;

    private float aggroTriggerTimer;

    public override EnemyStateType getStateType () {
        return EnemyStateType.IDLE;
    }

    public override EnemyStateType checkForNextState () {
        if (controller.playerIsNear && aggroTriggerTimer <= 0f) {
            Vector3 direction = controller.player.transform.position - transform.position;
            RaycastHit2D hit = Physics2D.Raycast (transform.position + (direction.normalized * raycastDirectionFactor), direction, maxRaycastCheckRange, obstaclesLayer);

            if (hit.collider?.gameObject == controller.player) {
                if (controller.aggroTriggerSoundPlayed) {
                    return aggressiveStateType;
                }

                aggroTriggerTimer = aggroTriggerDelay;

                FMOD.Studio.EventInstance pickupSoundEvent = FMODUnity.RuntimeManager.CreateInstance (aggroTriggerSound);
                pickupSoundEvent.set3DAttributes (FMODUnity.RuntimeUtils.To3DAttributes (transform.position));
                pickupSoundEvent.setPitch (aggroTriggerSoundPitch);
                pickupSoundEvent.start ();
                pickupSoundEvent.release ();
            }
        }

        return EnemyStateType.IDLE;
    }

    protected override void transitionTo () {
        if (controller != null) {
            controller.setMoveDirection (Vector2.zero);
        }
    }

    public override Vector3 getLookTarget () {
        return Vector3.zero;
    }

    private void Update () {
        if (aggroTriggerTimer > 0f) {
            aggroTriggerTimer -= Time.deltaTime;

            if (aggroTriggerTimer <= 0f) {
                controller.aggroTriggerSoundPlayed = true;
            }
        }
    }
}
