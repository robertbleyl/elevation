using FMODUnity;
using UnityEngine;

public class EnemyPouncePlayerStateController : AbstractEnemyPathingStateController {

    [SerializeField]
    private float pounceWindupTime = 1f;
    [SerializeField]
    private float pounceRange = 3f;
    [SerializeField]
    private float pounceDuration = 0.5f;
    [SerializeField]
    private float pounceSpeed = 100f;
    [SerializeField]
    private float pounceDamage = 20f;
    [SerializeField]
    private float pounceDamageEffectRadius = 1.5f;
    [SerializeField]
    private float pounceRecoveryTime = 2f;
    [SerializeField]
    private EnemyStateType followUpState = EnemyStateType.MELEE_ATTACK_PLAYER;
    [SerializeField]
    private EventReference pounceSound;

    private float windupTimer;
    private float durationTimer;
    private float recoveryTimer;

    private PIDController pidController = new PIDController (10f, 0f, 0.05f);

    private Rigidbody2D rigiBody2d;
    private Vector3 pounceTargetLocation;

    protected override void Awake () {
        base.Awake ();

        rigiBody2d = GetComponent<Rigidbody2D> ();
    }

    public override EnemyStateType getStateType () {
        return EnemyStateType.POUNCE_ATTACK_PLAYER;
    }

    public override EnemyStateType checkForNextState () {
        if (recoveryTimer >= pounceRecoveryTime) {
            return followUpState;
        }

        return EnemyStateType.POUNCE_ATTACK_PLAYER;
    }

    public override Vector3 getLookTarget () {
        if (controller == null || controller.player == null) {
            return Vector3.zero;
        }

        return controller.player.transform.position;
    }

    protected override Vector3 getNextTargetPosition () {
        if (controller == null || controller.player == null) {
            return Vector3.zero;
        }

        return controller.player.transform.position;
    }

    protected override bool shouldStartNewPath () {
        return controller.player != null;
    }

    protected override void transitionTo () {
        windupTimer = 0f;
        durationTimer = 0f;
        recoveryTimer = 0f;
    }

    protected override void FixedUpdate () {
        if (controller.stunned || controller.isDead ()) {
            return;
        }

        float distanceToPlayer = Vector3.Distance (getNextTargetPosition (), transform.position);

        if (windupTimer == 0f && distanceToPlayer > pounceRange) {
            base.FixedUpdate ();
        } else if (windupTimer < pounceWindupTime) {
            if (windupTimer == 0f) {
                pounceTargetLocation = getNextTargetPosition ();
                FMODUnity.RuntimeManager.PlayOneShotAttached (pounceSound.Guid, gameObject);
            }

            windupTimer += Time.deltaTime;
            controller.setMoveDirection (Vector2.zero);
        } else if (durationTimer < pounceDuration) {
            durationTimer += Time.deltaTime;

            if (durationTimer >= pounceDuration) {
                if (distanceToPlayer < pounceDamageEffectRadius) {
                    controller.player.GetComponent<PlayerHealthController> ().receiveDamage (pounceDamage);
                }
            } else {
                Vector2 targetSpeed = (pounceTargetLocation - transform.position) * pounceSpeed;
                Vector2 currentSpeed = rigiBody2d.velocity;

                Vector2 force = pidController.getCorrection (targetSpeed - currentSpeed);
                rigiBody2d.AddForce (force);
            }
        } else {
            recoveryTimer += Time.deltaTime;
            controller.setMoveDirection (Vector2.zero);
        }

    }
}
