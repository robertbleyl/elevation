using UnityEngine;

public abstract class AbstractEnemyStateController : MonoBehaviour {

    public enum EnemyStateType {
        IDLE, MOVE_IN_SHOOTING_RANGE, SHOOT_PLAYER, SEARCH_FOR_GUN, MELEE_ATTACK_PLAYER, RETREAT_FROM_PLAYER, POUNCE_ATTACK_PLAYER
    }

    protected EnemyController controller;
    protected EnemyProperties properties;
    protected int obstaclesLayer;

    protected virtual void Awake () {
        controller = GetComponent<EnemyController> ();
        properties = controller.properties;

        obstaclesLayer = LayerMask.GetMask (LayerConstants.Walls, LayerConstants.Characters, LayerConstants.StaticInterior);

        enabled = false;
    }

    public void activate () {
        enabled = true;
        transitionTo ();
    }

    public abstract EnemyStateType getStateType ();

    public abstract EnemyStateType checkForNextState ();

    public abstract Vector3 getLookTarget ();

    protected abstract void transitionTo ();
}
