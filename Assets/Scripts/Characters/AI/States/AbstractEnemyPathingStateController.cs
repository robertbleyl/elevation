using Pathfinding;
using UnityEngine;

public abstract class AbstractEnemyPathingStateController : AbstractEnemyStateController {

    [SerializeField]
    private float nextWaypointDistance = 3f;
    [SerializeField]
    private float pathUpdateInterval = 0.2f;

    private Seeker seeker;

    private float pathUpdateTimer;
    private Path currentPath;
    private int currentWaypointIndex;

    protected Vector2 moveDirection;

    protected override void Awake () {
        base.Awake ();

        seeker = GetComponent<Seeker> ();
    }

    private void OnPathCompleted (Path path) {
        if (path.error) {
            Debug.LogWarning ("OnPathCompleted error: " + path.errorLog);
            return;
        }

        currentPath = path;
        currentWaypointIndex = 0;
    }

    protected virtual bool pathNotReady () {
        return currentPath == null || currentWaypointIndex >= currentPath.vectorPath.Count;
    }

    protected Vector2 getMoveDirection () {
        return currentPath.vectorPath[currentWaypointIndex] - transform.position;
    }

    protected void updateWaypointIndex () {
        if (Vector2.Distance (transform.position, currentPath.vectorPath[currentWaypointIndex]) < nextWaypointDistance) {
            currentWaypointIndex++;
        }
    }

    protected virtual void FixedUpdate () {
        if (controller == null || controller.stunned || controller.isDead () || controller.player == null) {
            return;
        }

        pathUpdateTimer += Time.fixedDeltaTime;

        if (pathUpdateTimer >= pathUpdateInterval) {
            pathUpdateTimer = 0f;

            if (seeker.IsDone () && shouldStartNewPath ()) {
                seeker.StartPath (transform.position, getNextTargetPosition (), OnPathCompleted);
            }
        }

        if (pathNotReady ()) {
            return;
        }

        Vector2 moveDir = getMoveDirection ();
        controller.setMoveDirection (moveDir.normalized);

        updateWaypointIndex ();
    }

    protected abstract bool shouldStartNewPath ();

    protected abstract Vector3 getNextTargetPosition ();
}
