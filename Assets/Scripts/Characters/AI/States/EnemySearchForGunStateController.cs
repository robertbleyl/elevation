using UnityEngine;

public class EnemySearchForGunStateController : AbstractEnemyPathingStateController {

    [SerializeField]
    private float gunPickupRange = 1f;

    private GunController nearestGunForPickup;
    private Vector3 lastKnownLocationOfPickupGun;

    public override EnemyStateType getStateType () {
        return EnemyStateType.SEARCH_FOR_GUN;
    }

    public override EnemyStateType checkForNextState () {
        if (controller.equipedGun != null) {
            return EnemyStateType.MOVE_IN_SHOOTING_RANGE;
        }

        if (nearestGunForPickup == null) {
            return EnemyStateType.MELEE_ATTACK_PLAYER;
        }

        return EnemyStateType.SEARCH_FOR_GUN;
    }

    protected override void transitionTo () {
        tryToSearchForNewGun ();
    }

    private void tryToSearchForNewGun () {
        nearestGunForPickup = null;

        if (transform.parent == null || controller == null) {
            return;
        }

        GunController[] guns = transform.parent.GetComponentsInChildren<GunController> ();

        nearestGunForPickup = null;
        float bestDist = float.MaxValue;

        foreach (GunController gun in guns) {
            float dist = Vector3.Distance (gun.transform.position, transform.position);

            if (dist <= 4f && dist < bestDist) {
                bestDist = dist;
                nearestGunForPickup = gun;
            }
        }

        if (nearestGunForPickup != null) {
            lastKnownLocationOfPickupGun = nearestGunForPickup.transform.position;
        }
    }

    protected override bool shouldStartNewPath () {
        return nearestGunForPickup != null;
    }

    protected override Vector3 getNextTargetPosition () {
        return lastKnownLocationOfPickupGun;
    }

    public override Vector3 getLookTarget () {
        return lastKnownLocationOfPickupGun;
    }

    private void Update () {
        if (nearestGunForPickup != null) {
            if (lastKnownLocationOfPickupGun != nearestGunForPickup.transform.position) {
                tryToSearchForNewGun ();
            } else {
                tryToPickupNewGun ();
            }
        } else {
            tryToSearchForNewGun ();
        }
    }

    private void tryToPickupNewGun () {
        if (!controller.stunned && !controller.isDead () && Vector3.Distance (transform.position, nearestGunForPickup.transform.position) <= gunPickupRange) {
            controller.equipGun (nearestGunForPickup);
        }
    }
}
