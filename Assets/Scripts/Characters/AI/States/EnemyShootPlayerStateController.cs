using UnityEngine;

public class EnemyShootPlayerStateController : AbstractEnemyStateController {

    [SerializeField]
    private float shootTime = 0f;
    [SerializeField]
    private EnemyStateType shootTimeTransition = EnemyStateType.RETREAT_FROM_PLAYER;

    private float shootTimer;

    public override EnemyStateType getStateType () {
        return EnemyStateType.SHOOT_PLAYER;
    }

    public override EnemyStateType checkForNextState () {
        if (controller.equipedGun == null) {
            return EnemyStateType.SEARCH_FOR_GUN;
        }

        if (shootTime > 0f && shootTimer <= 0f) {
            return shootTimeTransition;
        }

        float distToPlayer = Vector3.Distance (transform.position, getLookTarget ());

        if (distToPlayer > properties.getMaxEngageRange ()) {
            return EnemyStateType.IDLE;
        }

        if (controller.equipedGun != null && distToPlayer > properties.getLeashShootRange ()) {
            return EnemyStateType.MOVE_IN_SHOOTING_RANGE;
        }

        return EnemyStateType.SHOOT_PLAYER;
    }

    protected override void transitionTo () {
        if (controller == null) {
            return;
        }

        controller.setMoveDirection (Vector2.zero);
        shootTimer = shootTime;
    }

    public override Vector3 getLookTarget () {
        if (controller == null || controller.player == null) {
            return Vector3.zero;
        }

        return controller.player.transform.position;
    }

    private void Update () {
        if (controller.stunned || controller.isDead () || controller.equipedGun == null) {
            return;
        }

        if (shootTimer > 0f) {
            shootTimer -= Time.deltaTime;
        }

        Vector3 direction = controller.player.transform.position - transform.position;
        RaycastHit2D hit = Physics2D.Raycast (transform.position + (direction.normalized * 1.5f), direction, 5f, obstaclesLayer);

        if (hit.collider?.gameObject == controller.player) {
            controller.rotateGunToTarget (Camera.main.WorldToScreenPoint (controller.player.transform.position));

            if (!controller.equipedGun.reloading) {
                controller.shoot (direction, false);

                if (controller.equipedGun.isEmpty ()) {
                    controller.equipedGun.reload (controller.equipedGun.ammoCapacity);
                }
            }
        }
    }
}
