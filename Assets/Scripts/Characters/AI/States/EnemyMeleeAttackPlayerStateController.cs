using UnityEngine;

public class EnemyMeleeAttackPlayerStateController : AbstractEnemyPathingStateController {

    [SerializeField]
    private float alternateAttackCooldown;
    [SerializeField]
    private EnemyStateType alternateAttackType = EnemyStateType.POUNCE_ATTACK_PLAYER;
    [SerializeField]
    private float meleeAttackEffectSpeed = 40f;

    private bool performingMeleeAttack;
    private float meleeAttackCooldownTimer;
    private float alternateAttackTimer;
    private float meleeAttackWindupTimer;
    private float meleeAttackRecoveryTimer;

    public override EnemyStateType getStateType () {
        return EnemyStateType.MELEE_ATTACK_PLAYER;
    }

    public override EnemyStateType checkForNextState () {
        if (alternateAttackCooldown > 0f && alternateAttackTimer >= alternateAttackCooldown) {
            return alternateAttackType;
        }

        return EnemyStateType.MELEE_ATTACK_PLAYER;
    }

    protected override void transitionTo () {
        meleeAttackCooldownTimer = 0f;
        alternateAttackTimer = 0f;
        meleeAttackWindupTimer = 0f;
        meleeAttackRecoveryTimer = 0f;
        performingMeleeAttack = false;
    }

    public override Vector3 getLookTarget () {
        if (controller == null || controller.player == null) {
            return Vector3.zero;
        }

        return controller.player.transform.position;
    }

    protected override Vector3 getNextTargetPosition () {
        if (controller == null || controller.player == null) {
            return transform.position;
        }

        return controller.player.transform.position;
    }

    protected override bool shouldStartNewPath () {
        return controller?.player != null;
    }

    protected override bool pathNotReady () {
        return base.pathNotReady () || meleeAttackRecoveryTimer > 0f || meleeAttackWindupTimer > 0f;
    }

    private void Update () {
        GameObject player = controller.player;

        if (player.GetComponent<PlayerController> ().isDead ()) {
            return;
        }

        if (controller.stunned) {
            transitionTo ();
            return;
        }

        if (alternateAttackCooldown > 0f) {
            alternateAttackTimer += Time.deltaTime;
        }

        if (meleeAttackRecoveryTimer > 0f) {
            meleeAttackRecoveryTimer -= Time.deltaTime;

            if (meleeAttackRecoveryTimer <= 0) {
                controller.stopMeleeAttack ();
            }
            return;
        }

        if (meleeAttackCooldownTimer > 0f) {
            meleeAttackCooldownTimer -= Time.deltaTime;
        } else {
            Vector3 toPlayer = player.transform.position - transform.position;
            bool playerIsInRange = toPlayer.magnitude <= properties.getMeleeRange ();

            if (performingMeleeAttack) {
                meleeAttackWindupTimer -= Time.deltaTime;
                controller.setMoveDirection (Vector2.zero);

                if (meleeAttackWindupTimer <= 0f) {
                    createMeleeEffect (toPlayer);

                    if (playerIsInRange) {
                        dealDamageToPlayer (player, toPlayer);
                    }

                    meleeAttackCooldownTimer = properties.getMeleeAttackCooldown ();
                    meleeAttackRecoveryTimer = properties.getMeleeAttackRecoveryTime ();
                    performingMeleeAttack = false;
                }
            } else if (playerIsInRange) {
                startMeleeAttack ();
            }
        }
    }

    private void createMeleeEffect (Vector3 toPlayer) {
        GameObject meleeEffect = Instantiate (properties.getMeleeEffectPrefab ());

        Vector3 sideWays = Vector2.Perpendicular (toPlayer / 2f);
        meleeEffect.transform.position = transform.position + (toPlayer / 2f) + sideWays;

        meleeEffect.transform.SetParent (transform.parent);
        meleeEffect.GetComponent<EnemyMeleeAttackEffectController> ().pushTarget = transform.position;

        Rigidbody2D effectRigidBody = meleeEffect.GetComponent<Rigidbody2D> ();
        effectRigidBody.AddForce ((-sideWays + (toPlayer.normalized / 2f)) * meleeAttackEffectSpeed);
    }

    private void dealDamageToPlayer (GameObject player, Vector3 toPlayer) {
        FMODUnity.RuntimeManager.PlayOneShotAttached (properties.getMeleeAttackHitSound ().Guid, gameObject);
        PlayerHealthController characterHealthController = player.GetComponent<PlayerHealthController> ();
        characterHealthController.receiveDamage (properties.getMeleeAttackDamage ());
        player.GetComponent<Rigidbody2D> ().AddForce (toPlayer.normalized * properties.getMeleeAttackForce ());
    }

    private void startMeleeAttack () {
        performingMeleeAttack = true;
        controller.performMeleeAttack ();
        meleeAttackWindupTimer = properties.getMeleeAttackWindupTime ();
        FMODUnity.RuntimeManager.PlayOneShotAttached (properties.getMeleeAttackSound ().Guid, gameObject);
    }
}
