using UnityEngine;

public class ArmorDropController : MonoBehaviour {

    [SerializeField]
    private Collider2D collider2d = null;

    public Collider2D originParentCollider;

    private bool done;

    private void Update () {
        if (done || collider2d == null) {
            return;
        }

        if (!originParentCollider.IsTouching (collider2d)) {
            Physics2D.IgnoreCollision (collider2d, originParentCollider, false);
            done = true;
        }
    }
}
