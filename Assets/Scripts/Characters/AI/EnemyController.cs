using System.Collections.Generic;
using UnityEngine;
using static AbstractEnemyStateController;

public class EnemyController : AbstractCharacterController<EnemyProperties> {

    [SerializeField]
    private EnemyStateType initialState = EnemyStateType.IDLE;
    [SerializeField]
    private Animator animator = null;
    [SerializeField]
    private Rigidbody2D rigidBody2d;
    [SerializeField]
    private SpriteRenderer[] spriteRenderers;

    public bool playerIsNear;
    public bool stunned;
    public float currentArmor;
    public float currentHitPoints;
    public Vector2 moveDirection;

    public bool aggroTriggerSoundPlayed;

    public GameObject player;

    private EnemyStateType currentState;
    private readonly Dictionary<EnemyStateType, AbstractEnemyStateController> states = new Dictionary<EnemyStateType, AbstractEnemyStateController> ();

    private NearestEnemy nearestEnemy;

    protected override void Start () {
        base.Start ();

        currentArmor = properties.getMaxArmor ();
        currentHitPoints = properties.getMaxHitPoints ();

        player = GameObject.FindGameObjectWithTag (Tags.Player);
        nearestEnemy = player.GetComponent<NearestEnemy> ();

        AbstractEnemyStateController[] stateList = GetComponents<AbstractEnemyStateController> ();

        foreach (AbstractEnemyStateController state in stateList) {
            states[state.getStateType ()] = state;
        }

        currentState = initialState;
        states[initialState].activate ();

        Events.instance.onLevelFinished += onLevelFinished;
    }

    private void onLevelFinished () {
        enabled = false;
    }

    private void OnDestroy () {
        Events.instance.onLevelFinished -= onLevelFinished;
    }

    private void OnBecameVisible () {
        playerIsNear = true;
    }

    private void OnBecameInvisible () {
        playerIsNear = false;
    }

    private void Update () {
        if (isDead () || player == null) {
            return;
        }

        if (playerIsNear) {
            nearestEnemy.checkEnemy (gameObject);
        }

        if (stunned) {
            return;
        }

        EnemyStateType nextState = states[currentState].checkForNextState ();

        if (nextState != currentState) {
            states[currentState].enabled = false;
            // Debug.Log ("switching from " + currentState + " to " + nextState);
            currentState = nextState;
            states[currentState].activate ();
        }

        Vector3 lookTarget = states[currentState].getLookTarget ();

        if (lookTarget != Vector3.zero) {
            Vector2 lookDirection = lookTarget - transform.position;
            looksLeft = lookDirection.x <= 0f;

            foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
                spriteRenderer.flipX = !looksLeft;
            }
        }
    }

    private void FixedUpdate () {
        if (stunned || isDead ()) {
            return;
        }

        rigidBody2d.MovePosition (rigidBody2d.position + moveDirection * properties.getMovementSpeed () * Time.fixedDeltaTime);
    }

    public void setMoveDirection (Vector2 direction) {
        if (!isDead ()) {
            animator.SetFloat ("Speed", moveDirection.sqrMagnitude);
            moveDirection = direction.normalized;
        }
    }

    public void performMeleeAttack () {
        if (!isDead ()) {
            animator.SetBool ("MeleeAttack", true);
        }
    }

    public void stopMeleeAttack () {
        animator.SetBool ("MeleeAttack", false);
    }

    public override bool isDead () {
        return currentHitPoints <= 0f;
    }
}
