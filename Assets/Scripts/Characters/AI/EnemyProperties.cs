using FMODUnity;
using UnityEngine;

[CreateAssetMenu (fileName = "EnemyProperties", menuName = "EnemyProperties")]
public class EnemyProperties : AbstractCharacterProperties {

    [SerializeField]
    private float startingShootRange = 4f;
    [SerializeField]
    private float leashShootRange = 5f;
    [SerializeField]
    private float maxEngageRange = 7f;

    [SerializeField]
    private EventReference meleeAttackSound;
    [SerializeField]
    private EventReference meleeAttackHitSound;
    [SerializeField]
    private float meleeAttackDamage = 10;
    [SerializeField]
    private float meleeAttackForce = 100000;
    [SerializeField]
    private float meleeAttackWindupTime = 0.5f;
    [SerializeField]
    private float meleeAttackRecoveryTime = 0.5f;
    [SerializeField]
    private float meleeAttackCooldown = 2f;
    [SerializeField]
    private GameObject meleeEffectPrefab = null;

    [SerializeField]
    private float maxHitPoints = 20f;

    [SerializeField]
    private float maxArmor = 20f;
    [SerializeField]
    private float armorRegenerationDelay = 1.5f;
    [SerializeField]
    private float armorRegenerationPerSecond = 2f;

    [SerializeField]
    private float gunLoseProbability = 0.666f;

    [SerializeField]
    private EventReference deathMeleeSound;
    [SerializeField]
    private float deathMeleeTime = 2f;

    public float getStartingShootRange () {
        return startingShootRange;
    }

    public float getLeashShootRange () {
        return leashShootRange;
    }

    public float getMaxEngageRange () {
        return maxEngageRange;
    }

    public EventReference getDeathMeleeSound () {
        return deathMeleeSound;
    }

    public float getDeathMeleeTime () {
        return deathMeleeTime;
    }

    public EventReference getMeleeAttackSound () {
        return meleeAttackSound;
    }

    public EventReference getMeleeAttackHitSound () {
        return meleeAttackHitSound;
    }

    public float getMeleeAttackDamage () {
        return meleeAttackDamage;
    }

    public float getMeleeAttackForce () {
        return meleeAttackForce;
    }

    public float getMeleeAttackWindupTime () {
        return meleeAttackWindupTime;
    }

    public float getMeleeAttackRecoveryTime () {
        return meleeAttackRecoveryTime;
    }

    public float getMeleeAttackCooldown () {
        return meleeAttackCooldown;
    }

    public GameObject getMeleeEffectPrefab () {
        return meleeEffectPrefab;
    }

    public float getGunLoseProbability () {
        return gunLoseProbability;
    }

    public float getMaxHitPoints () {
        return maxHitPoints;
    }

    public float getMaxArmor () {
        return maxArmor;
    }

    public float getArmorRegenerationDelay () {
        return armorRegenerationDelay;
    }

    public float getArmorRegenerationPerSecond () {
        return armorRegenerationPerSecond;
    }
}