using System.Collections;
using FMODUnity;
using UnityEngine;
using static AbstractMovementState;

public class EnemyHealthController : AbstractCharacterHealthController<EnemyProperties, EnemyController> {

    [SerializeField]
    private SpriteRenderer mainSpriteRenderer = null;
    [SerializeField]
    private Shader mainShader = null;
    [SerializeField]
    private Shader outlineShader = null;
    [SerializeField]
    private Color outlineMeleeRangeColor;
    [SerializeField]
    private Color outlineStunnedColor;

    [SerializeField]
    private SpriteRenderer hitPointIndicatorRenderer = null;
    [SerializeField]
    private SpriteRenderer armorIndicatorRenderer = null;

    [SerializeField]
    private EventReference bulletArmorImpactSound;

    [SerializeField]
    private GameObject[] armorPlates;
    [SerializeField]
    private GameObject[] armorPlatePrefabs;
    [SerializeField]
    private float armorPlateDropForce = 200f;

    [SerializeField]
    private GameObject deathReplacementObjectPrefab = null;
    [SerializeField]
    private GameObject deathMeleeReplacementObjectPrefab1 = null;
    [SerializeField]
    private GameObject deathMeleeReplacementObjectPrefab2 = null;
    [SerializeField]
    private Vector3 deathMeleeReplacementObjectOffsetLeft1;
    [SerializeField]
    private Vector3 deathMeleeReplacementObjectOffsetLeft2;
    [SerializeField]
    private Vector3 deathMeleeReplacementObjectOffsetRight1;
    [SerializeField]
    private Vector3 deathMeleeReplacementObjectOffsetRight2;

    private NearestEnemy nearestEnemy;
    private float playerMeleeRange;

    private int activeArmorPlates;

    private float initialHitPointIndicatorWidth;

    private float lastHitTimer;

    protected override void Start () {
        base.Start ();

        mainSpriteRenderer.material.shader = mainShader;

        GameObject player = GameObject.FindGameObjectWithTag (Tags.Player);
        nearestEnemy = player.GetComponent<NearestEnemy> ();
        playerMeleeRange = player.GetComponent<PlayerController> ().properties.getMeleeRange ();

        initialHitPointIndicatorWidth = hitPointIndicatorRenderer.transform.localScale.x;
    }

    public void regainHitPoints (float amount) {
        controller.currentHitPoints += amount;
        updateHitPointIndicator ();
    }

    public void regainArmor (float amount) {
        controller.currentArmor += amount;
        updateActiveArmorPlates ();
        updateArmorIndicator ();
    }

    private void updateActiveArmorPlates () {
        activeArmorPlates = (int)Mathf.Ceil (armorPlates.Length * getCurrentArmorPercentage ());

        for (int i = 0; i < armorPlates.Length; i++) {
            armorPlates[i].SetActive (i < activeArmorPlates);
        }
    }

    private void Update () {
        if (controller.isDead ()) {
            return;
        }

        if (controller.stunned && controller.currentArmor <= 0f) {
            Color outlineColor = nearestEnemy.currentNearest == gameObject && nearestEnemy.currentNearestDist <= playerMeleeRange ? outlineMeleeRangeColor : outlineStunnedColor;
            mainSpriteRenderer.material.SetColor ("_OutlineColor", outlineColor);
        }

        if (lastHitTimer > 0f) {
            lastHitTimer -= Time.deltaTime;

            if (lastHitTimer <= 0f) {
                lastHitTimer = 0f;
            }
        } else if (controller.currentArmor < properties.getMaxArmor ()) {
            regainArmor (properties.getArmorRegenerationPerSecond () * Time.deltaTime);

            if (controller.currentArmor > properties.getMaxArmor ()) {
                controller.currentArmor = properties.getMaxArmor ();
            }
        }
    }

    protected void OnCollisionEnter2D (Collision2D collision) {
        if (controller.isDead ()) {
            return;
        }

        BulletController bulletController = collision.collider.GetComponent<BulletController> ();

        float damage = 0f;
        float armorDamage = 0f;
        bool wasObjectCollision = false;
        Vector3 direction = collision.relativeVelocity;

        if (bulletController != null) {
            damage = bulletController.getDamage ();
            armorDamage = bulletController.getArmorDamage ();

            if (controller.currentArmor > 0f) {
                FMODUnity.RuntimeManager.PlayOneShotAttached (bulletArmorImpactSound.Guid, gameObject);
            }
        } else {
            ObjectController objectController = collision.collider.GetComponent<ObjectController> ();

            if (objectController != null && (objectController.getActiveMovementStateType () == MovementStateType.THROW
                || objectController.getActiveMovementStateType () == MovementStateType.PULLING)) {

                wasObjectCollision = true;
                damage = objectController.properties.getHitPointDamage ();
                armorDamage = objectController.properties.getArmorDamage ();

                float stunTime = controller.currentArmor > 0f ? objectController.properties.getStunTimeAgainstArmor () : objectController.properties.getStunTime ();

                if (stunTime > 0f) {
                    stun (stunTime);
                }
            }
        }

        receiveDamage (damage, armorDamage, direction);

        if (wasObjectCollision) {
            dropGunOfEnemy ();
        }
    }

    private void stun (float stunTime) {
        controller.stunned = true;
        animator.SetBool ("Stunned", true);
        mainSpriteRenderer.material.shader = outlineShader;
        StartCoroutine (unStun (stunTime));
    }

    private IEnumerator unStun (float stunTime) {
        yield return new WaitForSecondsRealtime (stunTime);
        controller.stunned = false;
        mainSpriteRenderer.material.shader = mainShader;

        if (!controller.isDead ()) {
            animator.SetBool ("Stunned", false);
        }
    }

    private void dropGunOfEnemy () {
        if (controller.currentArmor <= 0f
            && controller.equipedGun != null
            && !controller.isDead ()
            && controller.equipedGun != null
            && rand.NextDouble () >= 1f - properties.getGunLoseProbability ()) {

            GunController gun = controller.equipedGun;
            controller.dropGunIfEquiped ();

            Vector3 direction = new Vector3 (rand.Next (10), rand.Next (10), rand.Next (10));
            gun.GetComponent<Rigidbody2D> ().AddForce (direction.normalized * 300f);
        }
    }

    public void receiveDamage (float damage, float armorDamage, Vector3 direction) {
        if (damage <= 0f || controller.isDead ()) {
            return;
        }

        lastHitTimer = properties.getArmorRegenerationDelay ();

        if (armorDamage > 0f && controller.currentArmor > 0f) {
            reduceArmor (armorDamage, direction);
            return;
        }

        if (!controller.isDead ()) {
            controller.currentHitPoints -= damage;
        }

        updateHitPointIndicator ();

        if (controller.isDead ()) {
            die ();
        }
    }

    private void reduceArmor (float armorDamage, Vector3 direction) {
        controller.currentArmor -= armorDamage;

        int oldHighestActiveArmorPlateIndex = getHighestActiveArmorPlateIndex ();
        updateActiveArmorPlates ();
        int newHighestActiveArmorPlateIndex = getHighestActiveArmorPlateIndex ();

        if (oldHighestActiveArmorPlateIndex >= 0) {
            for (int i = oldHighestActiveArmorPlateIndex; i > newHighestActiveArmorPlateIndex; i--) {
                GameObject armorPlate = Instantiate (armorPlatePrefabs[i]);
                armorPlate.transform.SetParent (transform.parent);
                armorPlate.transform.localPosition = Vector3.zero;
                armorPlate.transform.position = transform.position;

                Physics2D.IgnoreCollision (collider2d, armorPlate.GetComponent<Collider2D> ());
                armorPlate.GetComponent<ArmorDropController> ().originParentCollider = collider2d;

                armorPlate.GetComponent<Rigidbody2D> ().AddForce (direction.normalized * armorPlateDropForce);
            }
        }

        if (controller.currentArmor < 0f) {
            controller.currentArmor = 0f;
        }

        updateArmorIndicator ();
    }

    private int getHighestActiveArmorPlateIndex () {
        for (int i = armorPlates.Length - 1; i >= 0; i--) {
            if (armorPlates[i].activeSelf) {
                return i;
            }
        }

        return -1;
    }

    private void die () {
        controller.currentHitPoints = 0f;

        if (controller.equipedGun != null) {
            controller.dropGunIfEquiped ();
        }

        Events.instance.characterDied (new CharacterDeathEvent (gameObject, false, false));

        animator.SetBool ("Dead", true);
        rigidBody2d.simulated = false;
        collider2d.enabled = false;

        if (!deathSound.IsNull) {
            FMODUnity.RuntimeManager.PlayOneShotAttached (deathSound.Guid, gameObject);
        }

        if (hitPointIndicatorRenderer != null) {
            hitPointIndicatorRenderer.transform.parent.gameObject.SetActive (false);
        }

        if (armorIndicatorRenderer != null) {
            armorIndicatorRenderer.transform.parent.gameObject.SetActive (false);
        }

        if (deathReplacementObjectPrefab != null) {
            StartCoroutine (delayedCharacterReplacement ());
        }
    }

    protected void updateIndicatorRenderer (SpriteRenderer renderer, float current, float max) {
        if (renderer != null && max > 0f) {
            float percentage = current / max;
            float newWidth = percentage * initialHitPointIndicatorWidth;
            renderer.transform.localScale = new Vector3 (newWidth, renderer.transform.localScale.y, renderer.transform.localScale.z);
        }
    }

    public void killByMelee () {
        if (controller.isDead ()) {
            return;
        }

        controller.currentArmor = 0f;
        controller.currentHitPoints = 0f;

        Events.instance.characterDied (new CharacterDeathEvent (gameObject, true, false));

        if (controller.equipedGun != null) {
            controller.dropGunIfEquiped ();
        }

        animator.SetBool ("DeathMelee", true);
        rigidBody2d.simulated = false;
        collider2d.enabled = false;

        FMODUnity.RuntimeManager.PlayOneShotAttached (properties.getDeathMeleeSound ().Guid, gameObject);

        hitPointIndicatorRenderer.transform.parent.gameObject.SetActive (false);
        armorIndicatorRenderer.transform.parent.gameObject.SetActive (false);

        StartCoroutine (delayedCharacterReplacementMelee ());
    }

    private IEnumerator delayedCharacterReplacement () {
        yield return new WaitForSecondsRealtime (deathTime);

        spawnReplacementObject (deathReplacementObjectPrefab, Vector3.zero);

        Destroy (gameObject);
    }

    private IEnumerator delayedCharacterReplacementMelee () {
        yield return new WaitForSecondsRealtime (properties.getDeathMeleeTime ());

        if (controller.looksLeft) {
            spawnReplacementObject (deathMeleeReplacementObjectPrefab1, deathMeleeReplacementObjectOffsetLeft1);
            spawnReplacementObject (deathMeleeReplacementObjectPrefab2, deathMeleeReplacementObjectOffsetLeft2);
        } else {
            spawnReplacementObject (deathMeleeReplacementObjectPrefab1, deathMeleeReplacementObjectOffsetRight1);
            spawnReplacementObject (deathMeleeReplacementObjectPrefab2, deathMeleeReplacementObjectOffsetRight2);
        }

        Destroy (gameObject);
    }

    private void spawnReplacementObject (GameObject replacementObjectPrefab, Vector3 offset) {
        GameObject replacementObject = Instantiate (replacementObjectPrefab);
        replacementObject.transform.SetParent (transform.parent);
        replacementObject.transform.position = transform.position + offset;

        ObjectController objectController = replacementObject.GetComponent<ObjectController> ();
        objectController.objectBody.flipX = !controller.looksLeft;
        objectController.shadow.flipX = !controller.looksLeft;
    }

    private void updateArmorIndicator () {
        updateIndicatorRenderer (armorIndicatorRenderer, controller.currentArmor, properties.getMaxArmor ());
    }

    private void updateHitPointIndicator () {
        updateIndicatorRenderer (hitPointIndicatorRenderer, controller.currentHitPoints, properties.getMaxHitPoints ());
    }

    public float getCurrentHitPointPercentage () {
        return controller.currentHitPoints / properties.getMaxHitPoints ();
    }

    public float getCurrentArmorPercentage () {
        return controller.currentArmor / properties.getMaxArmor ();
    }
}
