using UnityEngine;

public class EnemyMeleeAttackEffectController : MonoBehaviour {

    [SerializeField]
    private float aliveTime = 0.5f;
    [SerializeField]
    private float pushForce = 1f;
    [SerializeField]
    private Rigidbody2D rigidBody = null;

    public Vector3 pushTarget;

    private float timer;

    private void Update () {
        timer += Time.deltaTime;

        rigidBody.AddForce ((pushTarget - transform.position) * pushForce);

        if (timer > aliveTime) {
            Destroy (gameObject);
        }
    }
}
