using FMODUnity;
using UnityEngine.SceneManagement;

public class MainMenuScreenController : AbstractScreenController {

    public void restartLevel () {
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
    }

    public void loadKitchenLevel () {
        loadLevel ("KitchenVerticalSlice");
    }

    public void loadDebugLevel () {
        loadLevel ("Debug");
    }

    private void loadLevel (string levelName) {
        RuntimeManager.CoreSystem.mixerSuspend ();
        RuntimeManager.CoreSystem.mixerResume ();

        SceneManager.LoadScene (levelName);
    }
}
