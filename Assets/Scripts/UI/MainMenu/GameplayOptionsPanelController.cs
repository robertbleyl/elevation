using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameplayOptionsPanelController : MonoBehaviour {

    [SerializeField]
    private Toggle autoThrowToggle = null;
    [SerializeField]
    private Toggle autoReloadToggle = null;

    private bool initDone;

    private void Start () {
        StartCoroutine (delayedStart ());
    }

    private IEnumerator delayedStart () {
        yield return new WaitForSecondsRealtime (1f);

        autoThrowToggle.isOn = GameplayOptions.autoThrowEnabled ();
        autoReloadToggle.isOn = GameplayOptions.autoReloadEnabled ();

        initDone = true;
    }

    public void toggleAutoThrow () {
        if (initDone) {
            GameplayOptions.setAutoThrow (autoThrowToggle.isOn);
        }
    }

    public void toggleAutoReload () {
        if (initDone) {
            GameplayOptions.setAutoReload (autoReloadToggle.isOn);
        }
    }
}
