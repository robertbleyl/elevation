using UnityEngine;

public abstract class AbstractScreenController : MonoBehaviour {

    [SerializeField]
    public ScreenProperties properties;

    protected CanvasGroup canvasGroup;

    protected virtual void Awake () {
        canvasGroup = GetComponent<CanvasGroup> ();
    }

    public virtual void activate () {
        canvasGroup.alpha = 1f;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;

        Cursor.SetCursor (properties.getCursor (), properties.getCursorHotSpot (), CursorMode.Auto);
        Cursor.lockState = properties.isPaused () ? CursorLockMode.None : CursorLockMode.Confined;

        if (properties.isPaused ()) {
            Time.timeScale = 0f;
        }
    }

    public virtual void deactivate () {
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;

        if (properties.isPaused ()) {
            Time.timeScale = 1f;
        }
    }
}
