using System.Collections.Generic;
using UnityEngine;

public class ScreensManager : MonoBehaviour {

    public enum ScreenType {
        MAIN_MENU, HUD, DIALOG, END
    }

    [SerializeField]
    private ScreenType initScreenType = ScreenType.HUD;

    private Dictionary<ScreenType, AbstractScreenController> screens = new Dictionary<ScreenType, AbstractScreenController> ();
    private ScreenType activeScreen;

    private InputMain controls;

    private void Awake () {
        controls = new InputMain ();
        controls.Player.ToggleMainMenu.performed += ctx => toggleMainMenu ();
    }

    private void Start () {
        AbstractScreenController[] controllers = GetComponentsInChildren<AbstractScreenController> ();

        foreach (AbstractScreenController screen in controllers) {
            screen.deactivate ();
            screens[screen.properties.getScreenType ()] = screen;
        }

        activeScreen = initScreenType;
        activateScreen (initScreenType);

        Events.instance.onChangeScreen += activateScreen;
        Events.instance.onLevelFinished += onLevelFinsihed;
        Events.instance.onCharacterDied += onCharacterDied;
    }

    private void toggleMainMenu () {
        ScreenType nextScreen = activeScreen == ScreenType.MAIN_MENU ? ScreenType.HUD : ScreenType.MAIN_MENU;
        Events.instance.changeScreen (nextScreen);
    }

    private void onLevelFinsihed () {
        Events.instance.changeScreen (ScreenType.END);
    }

    private void onCharacterDied (CharacterDeathEvent characterDeathEvent) {
        if (characterDeathEvent.isPlayer) {
            Events.instance.changeScreen (ScreenType.END);
        }
    }

    private void activateScreen (ScreenType type) {
        screens[activeScreen].deactivate ();
        activeScreen = type;
        screens[activeScreen].activate ();
    }

    private void OnEnable () {
        controls.Enable ();
    }

    private void OnDisable () {
        controls.Disable ();
    }
}
