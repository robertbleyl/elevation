using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreenController : AbstractScreenController {

    [SerializeField]
    private Text successText = null;
    [SerializeField]
    private Text failText = null;

    private void Start () {
        successText.gameObject.SetActive (false);
        failText.gameObject.SetActive (false);

        Events.instance.onLevelFinished += onLevelFinsihed;
        Events.instance.onCharacterDied += onCharacterDied;
    }

    private void onLevelFinsihed () {
        successText.gameObject.SetActive (true);
    }

    private void onCharacterDied (CharacterDeathEvent characterDeathEvent) {
        if (characterDeathEvent.isPlayer) {
            failText.gameObject.SetActive (true);
        }
    }

    public void restartGame () {
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
    }
}
