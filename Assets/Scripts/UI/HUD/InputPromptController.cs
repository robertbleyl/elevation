using UnityEngine;
using UnityEngine.UI;

public class InputPromptController : MonoBehaviour {

    [SerializeField]
    private RectTransform inputPrompt = null;
    [SerializeField]
    private Text keyText = null;
    [SerializeField]
    private Text labelText = null;

    private GameObject currentTarget;
    private Vector3 currentOffset;

    private void Start () {
        Events.instance.onDisplayInputPrompt += onDisplayInputPrompt;
        Events.instance.onHideInputPrompt += onHideInputPrompt;

        gameObject.SetActive (false);
    }

    private void onDisplayInputPrompt (DisplayInputPromptEvent evt) {
        gameObject.SetActive (true);

        currentTarget = evt.getTarget ();
        currentOffset = evt.getOffset ();

        keyText.text = evt.getKey ();
        labelText.text = evt.getLabel ();
    }

    private void onHideInputPrompt () {
        currentTarget = null;
        gameObject.SetActive (false);
    }

    private void Update () {
        if (currentTarget == null) {
            return;
        }

        Vector3 screenPos = Camera.main.WorldToScreenPoint (currentTarget.transform.position) + currentOffset;
        inputPrompt.position = screenPos;
    }
}
