using FMODUnity;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveTextController : MonoBehaviour {

    [SerializeField]
    private Text objectiveTextElement = null;

    [SerializeField]
    private EventReference objectiveCompletedSound;

    [SerializeField]
    private Vector2 expandedPosition = new Vector2 (0f, -90f);
    [SerializeField]
    private float expandTime = 3f;
    [SerializeField]
    private int expandedFontSize = 55;

    [SerializeField]
    private Vector2 collapsedPosition = new Vector2 (0f, 0f);
    [SerializeField]
    private float collapseSpeed = 2f;
    [SerializeField]
    private int collapsedFontSize = 45;

    private GameObject player;

    private float expandTimer;
    private float collapsePercentage;

    private void Start () {
        player = GameObject.FindGameObjectWithTag (Tags.Player);
        Events.instance.onObjectiveChanged += onObjectiveChanged;
    }

    private void onObjectiveChanged (string objectiveText) {
        FMODUnity.RuntimeManager.PlayOneShot (objectiveCompletedSound, player.transform.position);

        objectiveTextElement.text = objectiveText;

        objectiveTextElement.rectTransform.anchoredPosition = expandedPosition;
        objectiveTextElement.fontSize = expandedFontSize;

        expandTimer = expandTime;
        collapsePercentage = 0f;
    }

    private void Update () {
        if (expandTimer > 0f) {
            expandTimer -= Time.deltaTime;
        }

        if (expandTimer <= 0f && collapsePercentage < 1f) {
            collapsePercentage += Time.deltaTime * collapseSpeed;

            if (collapsePercentage > 1f) {
                collapsePercentage = 1f;
                objectiveTextElement.rectTransform.anchoredPosition = collapsedPosition;
                objectiveTextElement.fontSize = collapsedFontSize;
            } else {
                objectiveTextElement.rectTransform.anchoredPosition = Vector2.Lerp (expandedPosition, collapsedPosition, collapsePercentage);
                objectiveTextElement.fontSize = (int)(collapsedFontSize + (expandedFontSize - collapsedFontSize) * (1f - collapsePercentage));
            }
        }
    }
}
