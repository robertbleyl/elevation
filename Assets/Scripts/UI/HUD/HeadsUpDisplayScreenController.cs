using UnityEngine;
using UnityEngine.UI;

public class HeadsUpDisplayScreenController : AbstractScreenController {

    [SerializeField]
    private Image shieldPercentageImage = null;
    [SerializeField]
    private Image ammoPercentageImage = null;
    [SerializeField]
    private Image energyPercentageImage = null;

    private GameObject player;
    private PlayerHealthController playerHealthController;
    private PlayerController playerController;

    private void Start () {
        player = GameObject.FindGameObjectWithTag (Tags.Player);
        playerHealthController = player.GetComponent<PlayerHealthController> ();
        playerController = player.GetComponent<PlayerController> ();
    }

    private void Update () {
        shieldPercentageImage.fillAmount = playerHealthController.getCurrentShieldPercentage ();
        ammoPercentageImage.fillAmount = playerController.getAmmoPercentage ();
        energyPercentageImage.fillAmount = playerController.getEnergyPercentage ();
    }
}
