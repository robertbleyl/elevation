using UnityEngine;
using static ScreensManager;

[CreateAssetMenu (fileName = "ScreenProperties", menuName = "ScreenProperties")]
public class ScreenProperties : ScriptableObject {

    [SerializeField]
    private ScreenType screenType;
    [SerializeField]
    private Vector2 cursorHotSpot;
    [SerializeField]
    private Texture2D cursor = null;
    [SerializeField]
    private bool paused;

    public ScreenType getScreenType () {
        return screenType;
    }

    public Vector2 getCursorHotSpot () {
        return cursorHotSpot;
    }

    public Texture2D getCursor () {
        return cursor;
    }

    public bool isPaused () {
        return paused;
    }
}