using UnityEngine;

[CreateAssetMenu (fileName = "DialogItem", menuName = "DialogItem")]
public class DialogItem : ScriptableObject {

    [SerializeField]
    public Sprite imageLeft = null;
    [SerializeField]
    public Sprite imageRight = null;
    [SerializeField]
    public string text;
}
