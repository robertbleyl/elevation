using UnityEngine;
using UnityEngine.UI;
using static ScreensManager;

public class DialogScreenController : AbstractScreenController {

    [SerializeField]
    private Image characterImageLeft = null;
    [SerializeField]
    private Image characterImageRight = null;
    [SerializeField]
    private Text dialogText = null;

    private InputMain controls;

    private Dialog activeDialog;
    private int dialogItemIndex;
    private MusicController musicController;

    protected override void Awake () {
        base.Awake ();

        controls = new InputMain ();
        controls.Player.ContinueDialog.performed += ctx => continueDialog ();
    }

    private void Start () {
        musicController = GameObject.FindGameObjectWithTag (Tags.Basics).GetComponent<MusicController> ();
    }

    public override void activate () {
        base.activate ();

        if (!activeDialog.musicAfterDialog.IsNull) {
            musicController.stopMusic (2f);
        }

        playDialogItem (0);
        controls.Enable ();
    }

    public override void deactivate () {
        base.deactivate ();
        controls.Disable ();
    }

    public void setActiveDialog (Dialog dialog) {
        activeDialog = dialog;
    }

    public void continueDialog () {
        if (dialogItemIndex < activeDialog.items.Length - 1) {
            playDialogItem (dialogItemIndex + 1);
        } else {
            if (!activeDialog.musicAfterDialog.IsNull) {
                musicController.playMusic (activeDialog.musicAfterDialog);
                musicController.fadeMusicIn (2f);
            }

            Events.instance.changeScreen (ScreenType.HUD);
        }
    }

    private void playDialogItem (int index) {
        this.dialogItemIndex = index;

        DialogItem item = activeDialog.items[index];

        setCharacterImage (characterImageLeft, item.imageLeft);
        setCharacterImage (characterImageRight, item.imageRight);
        dialogText.text = item.text;
    }

    private void setCharacterImage (Image characterImage, Sprite sprite) {
        if (sprite != null) {
            characterImage.sprite = sprite;
            characterImage.enabled = true;
        } else {
            characterImage.enabled = false;
        }
    }
}