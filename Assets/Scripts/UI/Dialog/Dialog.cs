using FMODUnity;
using UnityEngine;

[CreateAssetMenu (fileName = "Dialog", menuName = "Dialog")]
public class Dialog : ScriptableObject {

    [SerializeField]
    public DialogItem[] items;
    [SerializeField]
    public EventReference musicAfterDialog;
}
