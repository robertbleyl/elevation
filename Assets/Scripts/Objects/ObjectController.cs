using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;
using static AbstractMovementState;

public class ObjectController : MonoBehaviour {

    [SerializeField]
    public ObjectProperties properties;
    [SerializeField]
    public SpriteRenderer objectBody = null;
    [SerializeField]
    public SpriteRenderer shadow = null;
    [SerializeField]
    public Rigidbody2D rigidBody = null;
    [SerializeField]
    private GameObject impactEffectPrefab = null;
    [SerializeField]
    private Sprite[] destructionStatesBody = new Sprite[0];
    [SerializeField]
    private int damage;
    [SerializeField]
    private Shader outlineShader = null;
    [SerializeField]
    private Shader standardShader = null;
    [SerializeField]
    public TrailRenderer trailEffect = null;

    private Dictionary<MovementStateType, AbstractMovementState> movementStates = new Dictionary<MovementStateType, AbstractMovementState> ();
    private MovementStateType activeMovementStateType = MovementStateType.NEUTRAL;

    public GameObject playerPulledObjectLocation;
    public float playerPullSpeed;
    public float throwForce;
    public GameObject playerGunEquipLocation;
    public PullingObjects pullingObjects;
    public RigidbodyType2D defaultBodyType;
    public float defaultDrag;
    public int defaultLayer;
    public Collider2D playerCollider;

    public Vector3 nextThrowDirection;

    private void Start () {
        GameObject player = GameObject.FindGameObjectWithTag (Tags.Player);
        playerCollider = player.GetComponent<Collider2D> ();

        PlayerController playerController = player.GetComponent<PlayerController> ();

        playerPulledObjectLocation = playerController.pulledObjectLocation;
        playerGunEquipLocation = playerController.gunEquipLocation;

        PlayerProperties playerProperties = playerController.properties;
        playerPullSpeed = playerProperties.getPullSpeed ();
        throwForce = playerProperties.getThrowForce ();

        defaultBodyType = rigidBody.bodyType;
        defaultDrag = rigidBody.drag;
        defaultLayer = gameObject.layer;

        pullingObjects = player.GetComponent<PullingObjects> ();

        AbstractMovementState[] states = GetComponents<AbstractMovementState> ();

        foreach (AbstractMovementState state in states) {
            movementStates[state.getType ()] = state;
        }

        changeMovementState (MovementStateType.NEUTRAL);

        updateSprites ();
        hideHightlight ();
        hideShadow ();
    }

    public void changeMovementState (MovementStateType type) {
        movementStates[activeMovementStateType].enabled = false;
        activeMovementStateType = type;
        movementStates[activeMovementStateType].activate ();
    }

    public void setupNextThrowDirection (Vector2 nextThrowDirection) {
        this.nextThrowDirection = nextThrowDirection;
    }

    public void updateSpriteSortingLayer (string layerName) {
        objectBody.sortingLayerID = SortingLayer.NameToID (layerName);

        SpriteRenderer[] children = objectBody.GetComponentsInChildren<SpriteRenderer> ();

        foreach (SpriteRenderer child in children) {
            child.sortingLayerID = SortingLayer.NameToID (layerName);
        }
    }

    public void updateObjectBodyPositionForShadow () {
        updateObjectBodyPositionForShadow (0f, properties.getPickupTargetHeight ());
    }

    public void updateObjectBodyPositionForShadow (float x, float y) {
        objectBody.transform.localPosition = objectBody.transform.InverseTransformPoint (objectBody.transform.position + new Vector3 (x, y, 0f));
    }

    private void Update () {
        if (!shadow.gameObject.activeSelf) {
            resetHeight ();
        }
    }

    // private void OnTriggerEnter2D (Collider2D collider) {
    //     if (collider.GetComponent<BulletController> () != null && GetComponent<GunController> () == null) {
    //         FMODUnity.RuntimeManager.PlayOneShot (properties.getWallImpactSound (), transform.position);

    //         registerHit ();
    //     }
    // }

    public void registerHit () {
        damage++;

        rigidBody.drag = defaultDrag * 3f;

        if (damage >= properties.getHitPoints ()) {
            if (impactEffectPrefab != null) {
                GameObject impactEffect = Instantiate (impactEffectPrefab);
                impactEffect.transform.SetParent (transform.parent);
                impactEffect.transform.position = transform.position;
                impactEffect.transform.rotation = transform.rotation;
            }

            Events.instance.objectDestroyed (gameObject);
            StartCoroutine (destroyObject ());
        } else if (destructionStatesBody.Length >= damage && destructionStatesBody[damage] != null) {
            updateSprites ();
        }
    }

    public void playEnemyImpactSound (GameObject enemy) {
        EventReference eventRef = enemy.GetComponent<EnemyController> ().currentArmor > 0f ? properties.getEnemyArmorImpactSound () : properties.getEnemyImpactSound ();
        FMODUnity.RuntimeManager.PlayOneShotAttached (eventRef.Guid, gameObject);
    }

    private void updateSprites () {
        objectBody.sprite = destructionStatesBody[damage];
        shadow.sprite = destructionStatesBody[damage];
    }

    public bool isVisible () {
        return objectBody.isVisible;
    }

    public void resetHeight () {
        objectBody.transform.localPosition = Vector3.zero;
    }

    public void showHightlight () {
        updateHighlight (outlineShader);
    }

    public void hideHightlight () {
        updateHighlight (standardShader);
    }

    private void updateHighlight (Shader shader) {
        objectBody.GetComponent<Renderer> ().material.shader = shader;
    }

    public void showShadow () {
        shadow.gameObject.SetActive (true);
    }

    public void hideShadow () {
        shadow.gameObject.SetActive (false);
    }

    private IEnumerator destroyObject () {
        yield return new WaitForSecondsRealtime (0.1f);
        Destroy (gameObject);
    }

    public Rigidbody2D getRigidbody () {
        return rigidBody;
    }

    public MovementStateType getActiveMovementStateType () {
        return activeMovementStateType;
    }

    public bool hasHitPointsLeft () {
        return damage < properties.getHitPoints ();
    }
}
