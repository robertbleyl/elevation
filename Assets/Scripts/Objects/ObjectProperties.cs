using FMODUnity;
using UnityEngine;

[CreateAssetMenu (fileName = "ObjectProperties", menuName = "ObjectProperties")]
public class ObjectProperties : ScriptableObject {

    [SerializeField]
    private bool canBePulled = true;
    [SerializeField]
    private bool canBeDamagedByBullets = true;

    [SerializeField]
    private float pickupTargetHeight = 0.05f;
    [SerializeField]
    private float pickupWobble = 0.1f;
    [SerializeField]
    private float pickupSpeed = 0.5f;
    [SerializeField]
    private float pickupSoundPitch = 1.1f;

    [SerializeField]
    private float stunTime = 1.5f;
    [SerializeField]
    private float stunTimeAgainstArmor = 0f;
    [SerializeField]
    private float hitPointDamage = 1f;
    [SerializeField]
    private float armorDamage = 1f;
    [SerializeField]
    private int hitPoints = 2;
    [SerializeField]
    private float requiredEnergyForPull = 15f;

    [SerializeField]
    private float minPickupTorque = 20f;
    [SerializeField]
    private float maxPickupTorque = 100f;
    [SerializeField]
    private float minThrowTorque = 20f;
    [SerializeField]
    private float maxThrowTorque = 100f;

    [SerializeField]
    private EventReference pullSound;
    [SerializeField]
    private EventReference throwSound;
    [SerializeField]
    private EventReference enemyImpactSound;
    [SerializeField]
    private EventReference enemyArmorImpactSound;
    [SerializeField]
    private EventReference wallImpactSound;
    [SerializeField]
    private EventReference objectImpactSound;

    public bool isCanBePulled () {
        return canBePulled;
    }

    public bool isCanBeDamagedByBullets () {
        return canBeDamagedByBullets;
    }

    public float getPickupTargetHeight () {
        return pickupTargetHeight;
    }

    public float getPickupWobble () {
        return pickupWobble;
    }

    public float getPickupSpeed () {
        return pickupSpeed;
    }

    public float getPickupSoundPitch () {
        return pickupSoundPitch;
    }

    public float getStunTime () {
        return stunTime;
    }

    public float getStunTimeAgainstArmor () {
        return stunTimeAgainstArmor;
    }

    public float getHitPointDamage () {
        return hitPointDamage;
    }

    public float getArmorDamage () {
        return armorDamage;
    }

    public int getHitPoints () {
        return hitPoints;
    }

    public float getRequiredEnergyForPull () {
        return requiredEnergyForPull;
    }

    public float getMinPickupTorque () {
        return minPickupTorque;
    }

    public float getMaxPickupTorque () {
        return maxPickupTorque;
    }

    public float getMinThrowTorque () {
        return minThrowTorque;
    }

    public float getMaxThrowTorque () {
        return maxThrowTorque;
    }

    public EventReference getPullSound () {
        return pullSound;
    }

    public EventReference getThrowSound () {
        return throwSound;
    }

    public EventReference getEnemyImpactSound () {
        return enemyImpactSound;
    }

    public EventReference getEnemyArmorImpactSound () {
        return enemyArmorImpactSound;
    }

    public EventReference getWallImpactSound () {
        return wallImpactSound;
    }

    public EventReference getObjectImpactSound () {
        return objectImpactSound;
    }
}