using System.Collections;
using FMODUnity;
using UnityEngine;
using static AbstractMovementState;

public class GunController : MonoBehaviour {

    private static System.Random rand = new System.Random ();

    [SerializeField]
    private float playerFireRate = 0.5f;
    [SerializeField]
    private float enemyFireRate = 1f;
    [SerializeField]
    private float reloadTime = 1f;
    [SerializeField]
    public int ammoCapacity = 10;
    [SerializeField]
    private SpriteRenderer ammoIndicatorRenderer = null;
    [SerializeField]
    private Transform gunMuzzleLocation = null;
    [SerializeField]
    private GameObject bulletPrefab = null;
    [SerializeField]
    private int bulletCount = 1;
    [SerializeField]
    private int spread = 1;
    [SerializeField]
    public GunType gunType;
    [SerializeField]
    private float playerBulletSpeed = 0.1f;
    [SerializeField]
    private float enemyBulletSpeed = 0.01f;
    [SerializeField]
    private EventReference fireSound;
    [SerializeField]
    private EventReference equipSound;
    [SerializeField]
    private EventReference reloadSound;
    [SerializeField]
    private ParticleSystem[] muzzleFlashs;

    private GameObject currentHandSprite;

    private float currentCoolDown;
    private int availableAmmo;

    private float initialAmmoIndicatorWidth;

    public bool reloading;

    private void Start () {
        Collider2D player = GameObject.FindGameObjectWithTag (Tags.Player).GetComponent<Collider2D> ();
        Physics2D.IgnoreCollision (player, GetComponent<Collider2D> ());

        initialAmmoIndicatorWidth = ammoIndicatorRenderer.transform.localScale.x;

        setAmmo (ammoCapacity);
    }

    private void Update () {
        currentCoolDown -= Time.deltaTime;

        if (currentCoolDown <= 0f) {
            currentCoolDown = 0f;
        }
    }

    public void setAmmo (int ammo) {
        availableAmmo = ammo;

        float percentage = (float)availableAmmo / (float)ammoCapacity;

        float newWidth = percentage * initialAmmoIndicatorWidth;
        ammoIndicatorRenderer.transform.localScale = new Vector3 (newWidth, ammoIndicatorRenderer.transform.localScale.y, ammoIndicatorRenderer.transform.localScale.z);
    }

    public void reload (int ammo) {
        if (reloading) {
            return;
        }

        reloading = true;

        FMOD.Studio.EventInstance pickupSoundEvent = FMODUnity.RuntimeManager.CreateInstance (reloadSound);
        pickupSoundEvent.set3DAttributes (FMODUnity.RuntimeUtils.To3DAttributes (transform.position));
        pickupSoundEvent.setVolume (playerHasThisGun () ? 1f : 0.7f);
        pickupSoundEvent.start ();
        pickupSoundEvent.release ();

        StartCoroutine (finishReloading (ammo));
    }

    private IEnumerator finishReloading (int ammo) {
        yield return new WaitForSecondsRealtime (reloadTime);

        setAmmo (ammo);
        reloading = false;
    }

    private bool playerHasThisGun () {
        return GetComponentInParent<PlayerController> () != null;
    }

    public void equip (GameObject gunEquipLocation, GameObject handsPrefab, bool silent) {
        transform.SetParent (gunEquipLocation.transform);
        GetComponent<ObjectController> ().changeMovementState (MovementStateType.EQUIPPED);

        GameObject hands = Instantiate (handsPrefab);
        attachHandsSprite (hands);

        if (!silent) {
            FMODUnity.RuntimeManager.PlayOneShot (equipSound, transform.position);
        }
    }

    public void fireGun (Vector2 direction, bool player) {
        if (currentCoolDown > 0f || availableAmmo == 0 || transform.parent == null || transform.parent.parent == null) {
            return;
        }

        currentCoolDown = player ? playerFireRate : enemyFireRate;
        setAmmo (availableAmmo - 1);

        float bulletSpeed = player ? playerBulletSpeed : enemyBulletSpeed;

        for (int i = 0; i < bulletCount; i++) {
            Vector2 dir = direction + new Vector2 (randCoord (), randCoord ());

            GameObject bullet = Instantiate (bulletPrefab);
            bullet.transform.SetParent (transform.parent.parent);
            bullet.transform.position = gunMuzzleLocation.position;
            bullet.transform.rotation = transform.rotation;
            bullet.GetComponent<Rigidbody2D> ().AddForce (direction.normalized * bulletSpeed);
            Collider2D collider1 = transform.parent.parent.GetComponent<Collider2D> ();

            if (collider1 != null) {
                Physics2D.IgnoreCollision (collider1, bullet.GetComponent<Collider2D> ());
            }
        }

        foreach (ParticleSystem flash in muzzleFlashs) {
            flash.Play ();
        }

        FMODUnity.RuntimeManager.PlayOneShotAttached (fireSound.Guid, gameObject);
    }

    private float randCoord () {
        return rand.Next (spread) * (rand.Next (0, 1) == 0 ? -1 : 1);
    }

    public bool isEmpty () {
        return availableAmmo == 0;
    }

    public int getAvailableAmmo () {
        return availableAmmo;
    }

    public void attachHandsSprite (GameObject handsSprite) {
        detachHandsSprite ();
        currentHandSprite = handsSprite;

        currentHandSprite.transform.SetParent (transform);
        currentHandSprite.transform.localPosition = Vector3.zero;
        currentHandSprite.transform.localScale = Vector3.one;
        currentHandSprite.transform.localRotation = new Quaternion ();
    }

    public void detachHandsSprite () {
        if (currentHandSprite != null) {
            Destroy (currentHandSprite);
            currentHandSprite = null;
        }
    }
}
