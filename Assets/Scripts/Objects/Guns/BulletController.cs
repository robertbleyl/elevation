using UnityEngine;

public class BulletController : MonoBehaviour {

    [SerializeField]
    private float timeToLive = 3f;
    [SerializeField]
    private float damage = 10f;
    [SerializeField]
    private float armorDamage = 0f;
    [SerializeField]
    private GameObject impactEffectPrefab = null;

    private float aliveTimer;

    private void Update () {
        aliveTimer += Time.deltaTime;

        if (aliveTimer >= timeToLive) {
            Destroy (gameObject);
        }
    }

    private void OnCollisionEnter2D (Collision2D collision) {
        destroyBullet ();
    }

    public void destroyBullet () {
        GameObject impactEffect = Instantiate (impactEffectPrefab);
        impactEffect.transform.SetParent (transform.parent);
        impactEffect.transform.position = transform.position;
        impactEffect.transform.rotation = transform.rotation;

        Destroy (gameObject);
    }

    public float getDamage () {
        return damage;
    }

    public float getArmorDamage () {
        return armorDamage;
    }
}
