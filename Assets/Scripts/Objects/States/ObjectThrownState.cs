using UnityEngine;

public class ObjectThrownState : AbstractMovementState {

    private bool hadVelocityAfterThrowInit;
    private bool wasHitInThisThrow;

    public override MovementStateType getType () {
        return MovementStateType.THROW;
    }

    protected override void transitionTo () {
        GetComponent<Collider2D> ().isTrigger = false;
        gameObject.layer = LayerMask.NameToLayer (LayerConstants.ThrownObjects);
        transform.SetParent (objectController.playerPulledObjectLocation.transform.parent.parent);

        rigidBody.AddTorque (Random.Range (properties.getMinThrowTorque (), properties.getMaxThrowTorque ()), ForceMode2D.Impulse);

        rigidBody.AddForce (objectController.nextThrowDirection.normalized * objectController.throwForce);

        objectController.nextThrowDirection = Vector3.zero;

        objectController.updateSpriteSortingLayer (SortingLayerConstants.Objects);

        FMODUnity.RuntimeManager.PlayOneShot (properties.getThrowSound (), transform.position);

        hadVelocityAfterThrowInit = false;
        wasHitInThisThrow = false;
        objectController.playerCollider.GetComponent<PlayerController> ().pulledObject = null;

        objectController.trailEffect.gameObject.SetActive (true);
    }

    private void Update () {
        if (!hadVelocityAfterThrowInit && objectController.rigidBody.velocity.magnitude > 0.01f) {
            hadVelocityAfterThrowInit = true;
        } else if (hadVelocityAfterThrowInit && objectController.rigidBody.velocity.magnitude < 0.5f && objectController.hasHitPointsLeft ()) {
            objectController.changeMovementState (MovementStateType.NEUTRAL);
        }

        objectController.updateObjectBodyPositionForShadow ();
    }

    private void OnCollisionEnter2D (Collision2D collision) {
        if (enabled && !wasHitInThisThrow) {
            if (collision.collider.GetComponent<EnemyHealthController> () != null) {
                objectController.playEnemyImpactSound (collision.collider.gameObject);
            } else if (collision.collider.GetComponent<ObjectController> () != null) {
                FMODUnity.RuntimeManager.PlayOneShot (properties.getObjectImpactSound (), transform.position);
            } else {
                FMODUnity.RuntimeManager.PlayOneShot (properties.getWallImpactSound (), transform.position);
            }

            objectController.registerHit ();
            wasHitInThisThrow = true;
        }
    }
}
