using UnityEngine;

public abstract class AbstractMovementState : MonoBehaviour {

    public enum MovementStateType {
        NEUTRAL, PICKING_UP, PULLING, CAUGHT, THROW, EQUIPPED
    }

    protected ObjectController objectController;
    protected ObjectProperties properties;
    protected SpriteRenderer objectBody;
    protected SpriteRenderer shadow;
    protected Rigidbody2D rigidBody;

    protected virtual void Awake () {
        enabled = false;
        objectController = GetComponent<ObjectController> ();
        properties = objectController.properties;
        objectBody = objectController.objectBody;
        shadow = objectController.shadow;
        rigidBody = objectController.rigidBody;
    }

    public abstract MovementStateType getType ();

    public void activate () {
        enabled = true;
        transitionTo ();
    }

    protected abstract void transitionTo ();
}
