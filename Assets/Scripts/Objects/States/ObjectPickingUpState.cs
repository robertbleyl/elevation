using UnityEngine;

public class ObjectPickingUpState : AbstractMovementState {

    private float height;

    public override MovementStateType getType () {
        return MovementStateType.PICKING_UP;
    }

    protected override void transitionTo () {
        height = 0f;

        FMOD.Studio.EventInstance pickupSoundEvent = FMODUnity.RuntimeManager.CreateInstance (properties.getPullSound ());
        pickupSoundEvent.set3DAttributes (FMODUnity.RuntimeUtils.To3DAttributes (transform.position));
        pickupSoundEvent.setPitch (properties.getPickupSoundPitch ());
        pickupSoundEvent.start ();
        pickupSoundEvent.release ();

        objectController.showShadow ();
        objectController.hideHightlight ();
        gameObject.layer = LayerMask.NameToLayer (LayerConstants.PullingInObjects);
        Physics2D.IgnoreCollision (GetComponent<Collider2D> (), objectController.playerCollider);
    }

    private void Update () {
        height += Time.deltaTime * properties.getPickupSpeed ();
        float wobbleX = Random.Range (-properties.getPickupWobble (), properties.getPickupWobble ());

        shadow.transform.localPosition = shadow.transform.InverseTransformPoint (shadow.transform.position + new Vector3 (wobbleX, 0f, 0f));
        objectController.updateObjectBodyPositionForShadow (wobbleX, height);

        if (height >= properties.getPickupTargetHeight ()) {
            rigidBody.bodyType = RigidbodyType2D.Dynamic;
            rigidBody.mass = 1f;
            rigidBody.drag = 3f;

            objectBody.transform.localPosition = objectBody.transform.InverseTransformPoint (objectBody.transform.position + new Vector3 (0f, height, 0f));
            shadow.transform.localPosition = Vector3.zero;

            if (objectController.nextThrowDirection != Vector3.zero) {
                objectController.changeMovementState (MovementStateType.THROW);
            } else {
                objectController.changeMovementState (MovementStateType.PULLING);
            }
        }
    }
}
