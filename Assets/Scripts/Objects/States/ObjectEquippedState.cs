using UnityEngine;

public class ObjectEquippedState : AbstractMovementState {

    public override MovementStateType getType () {
        return MovementStateType.EQUIPPED;
    }

    protected override void transitionTo () {
        transform.localPosition = Vector3.zero;
        GetComponent<Collider2D> ().enabled = false;
        rigidBody.simulated = false;
        objectController.hideShadow ();
        objectController.resetHeight ();
        objectController.updateSpriteSortingLayer (SortingLayerConstants.EquippedObjects);

        objectController.trailEffect.gameObject.SetActive (false);
    }
}
