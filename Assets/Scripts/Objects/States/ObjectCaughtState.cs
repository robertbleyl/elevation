using UnityEngine;

public class ObjectCaughtState : AbstractMovementState {

    private const float lastTorqueCheckTime = 0.2f;

    private Vector3 lastTorqueCheckPosition;
    private float lastTorqueCheckTimer;

    public override MovementStateType getType () {
        return MovementStateType.CAUGHT;
    }

    protected override void transitionTo () {
        transform.SetParent (objectController.playerPulledObjectLocation.transform);
        transform.localPosition = Vector3.zero;
        GetComponent<Collider2D> ().isTrigger = true;
        rigidBody.velocity = Vector3.zero;
        gameObject.layer = LayerMask.NameToLayer (LayerConstants.PickedUpObjects);
        objectController.updateSpriteSortingLayer (SortingLayerConstants.CaughtObjects);

        objectController.trailEffect.gameObject.SetActive (false);

        GunController gun = GetComponent<GunController> ();

        if (gun != null) {
            objectController.playerCollider.GetComponent<PlayerController> ().addAmmoFromGun (gun);
        }

        lastTorqueCheckPosition = objectController.playerPulledObjectLocation.transform.localPosition;
        lastTorqueCheckTimer = lastTorqueCheckTime;
    }

    private void Update () {
        transform.localPosition = Vector3.zero;

        lastTorqueCheckTimer -= Time.deltaTime;

        if (lastTorqueCheckTimer <= 0f) {
            lastTorqueCheckTimer = lastTorqueCheckTime;

            Vector3 direction = lastTorqueCheckPosition - objectController.playerPulledObjectLocation.transform.localPosition;
            lastTorqueCheckPosition = objectController.playerPulledObjectLocation.transform.localPosition;

            if (direction.magnitude >= 0.1f) {
                rigidBody.AddTorque (direction.x * 10f, ForceMode2D.Impulse);
            }
        }

        objectController.updateObjectBodyPositionForShadow ();
    }

    private void OnTriggerEnter2D (Collider2D collider) {
        if (!enabled) {
            return;
        }

        BulletController bulletController = collider.GetComponent<BulletController> ();

        if (bulletController != null) {
            objectController.registerHit ();
            bulletController.destroyBullet ();
        }
    }
}
