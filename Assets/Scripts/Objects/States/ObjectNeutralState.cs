using UnityEngine;

public class ObjectNeutralState : AbstractMovementState {

    public override MovementStateType getType () {
        return MovementStateType.NEUTRAL;
    }

    protected override void transitionTo () {
        if (!properties.isCanBePulled ()) {
            return;
        }

        rigidBody.bodyType = objectController.defaultBodyType;
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = 0f;
        rigidBody.simulated = true;
        rigidBody.drag = objectController.defaultDrag;
        gameObject.layer = objectController.defaultLayer;

        objectController.trailEffect.gameObject.SetActive (false);

        Collider2D objectCollider = GetComponent<Collider2D> ();
        objectCollider.enabled = true;

        Physics2D.IgnoreCollision (objectCollider, objectController.playerCollider, false);

        if (objectController.GetComponent<GunController> () != null) {
            objectCollider.isTrigger = true;
            gameObject.layer = LayerMask.NameToLayer (LayerConstants.HeavyObjects);
            objectController.updateSpriteSortingLayer (SortingLayerConstants.GunOnGround);
        } else {
            objectController.updateSpriteSortingLayer (SortingLayerConstants.Objects);
        }

        objectController.hideShadow ();
        objectController.resetHeight ();
    }

    private void Update () {
        if (objectController.isVisible () && properties.isCanBePulled ()) {
            objectController.pullingObjects.checkObject (objectController);
        }
    }

    private void OnCollisionEnter2D (Collision2D collision) {
        if (!enabled) {
            return;
        }

        if ((collision.collider.GetComponent<BulletController> () != null && properties.isCanBeDamagedByBullets ()) ||
        (collision.collider.GetComponent<ObjectController> () != null && collision.collider.GetComponent<ObjectController> ().getActiveMovementStateType () == MovementStateType.THROW)) {
            objectController.registerHit ();
        }
    }
}
