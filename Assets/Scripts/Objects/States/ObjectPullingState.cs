using UnityEngine;

public class ObjectPullingState : AbstractMovementState {

    private bool wasHitInThisPull;

    public override MovementStateType getType () {
        return MovementStateType.PULLING;
    }

    protected override void transitionTo () {
        wasHitInThisPull = false;
        objectController.trailEffect.gameObject.SetActive (true);
        rigidBody.AddTorque (Random.Range (properties.getMinPickupTorque (), properties.getMaxPickupTorque ()), ForceMode2D.Impulse);
    }

    private void Update () {
        objectController.updateObjectBodyPositionForShadow ();
    }

    private void FixedUpdate () {
        Vector2 toPulledObject = (Vector2)objectController.playerPulledObjectLocation.transform.position - rigidBody.position;
        float dist = toPulledObject.magnitude;

        if (dist <= 0.3f) {
            objectController.changeMovementState (MovementStateType.CAUGHT);
        } else {
            rigidBody.AddForce (toPulledObject.normalized * objectController.playerPullSpeed);
        }
    }

    private void OnCollisionEnter2D (Collision2D collision) {
        if (enabled && !wasHitInThisPull && collision.collider.GetComponent<EnemyHealthController> () != null) {
            objectController.playEnemyImpactSound (collision.collider.gameObject);

            objectController.registerHit ();
            wasHitInThisPull = true;
        }
    }
}
