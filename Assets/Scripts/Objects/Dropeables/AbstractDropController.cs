using FMODUnity;
using UnityEngine;

public abstract class AbstractDropController : MonoBehaviour {

    [SerializeField]
    protected EventReference pickupSound;

    protected GameObject player;

    protected virtual void Start () {
        player = GameObject.FindGameObjectWithTag (Tags.Player);
    }

    protected abstract bool shouldPickup ();

    protected abstract void performPickup ();

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.gameObject == player && shouldPickup ()) {
            performPickup ();

            if (!pickupSound.IsNull) {
                FMODUnity.RuntimeManager.PlayOneShot (pickupSound, transform.position);
            }

            Destroy (gameObject);
        }
    }
}