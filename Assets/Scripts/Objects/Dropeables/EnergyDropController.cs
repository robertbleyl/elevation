using UnityEngine;

public class EnergyDropController : AbstractDropController {

    [SerializeField]
    private float energyAmount = 10f;

    private PlayerHealthController playerHealthController;

    protected override void Start () {
        base.Start ();
        playerHealthController = player.GetComponent<PlayerHealthController> ();
    }

    protected override bool shouldPickup () {
        return playerHealthController.getCurrentShieldPercentage () < 1f;
    }

    protected override void performPickup () {
        playerHealthController.regainShield (energyAmount);
    }
}
