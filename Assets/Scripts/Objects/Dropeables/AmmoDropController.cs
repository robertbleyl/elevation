using UnityEngine;

public class AmmoDropController : AbstractDropController {

    [SerializeField]
    public int ammoAmount = 2;

    private PlayerController playerController;
    private PlayerProperties playerProperties;

    protected override void Start () {
        base.Start ();
        playerController = player.GetComponent<PlayerController> ();
        playerProperties = playerController.properties;
    }

    protected override bool shouldPickup () {
        return playerProperties.getMaxAmmo () - playerController.currentAmmo >= ammoAmount;
    }

    protected override void performPickup () {
        playerController.addAmmoFromDrop (this);
    }
}
