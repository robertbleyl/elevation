using UnityEngine;
using System;
using static ScreensManager;

public class Events : MonoBehaviour {

    public static Events instance;

    private void Awake () {
        instance = this;
    }

    public event Action onLevelFinished;

    public event Action<ScreenType> onChangeScreen;

    public event Action<CharacterDeathEvent> onCharacterDied;

    public event Action<EquipGunEvent> onEquipGun;

    public event Action<GameObject> onObjectDestroyed;

    public event Action<string> onObjectiveChanged;

    public event Action<DisplayInputPromptEvent> onDisplayInputPrompt;

    public event Action onHideInputPrompt;

    public void levelFinished () {
        if (onLevelFinished != null) {
            onLevelFinished ();
        }
    }

    public void characterDied (CharacterDeathEvent characterDeathEvent) {
        if (onCharacterDied != null) {
            onCharacterDied (characterDeathEvent);
        }
    }

    public void equipGun (EquipGunEvent equipGunEvent) {
        if (onEquipGun != null) {
            onEquipGun (equipGunEvent);
        }
    }

    public void changeScreen (ScreenType screenType) {
        if (onChangeScreen != null) {
            onChangeScreen (screenType);
        }
    }

    public void objectDestroyed (GameObject obj) {
        if (onObjectDestroyed != null) {
            onObjectDestroyed (obj);
        }
    }

    public void objectiveChanged (string objectiveText) {
        if (onObjectiveChanged != null) {
            onObjectiveChanged (objectiveText);
        }
    }

    public void displayInputPrompt (DisplayInputPromptEvent displayInputPromptEvent) {
        if (onDisplayInputPrompt != null) {
            onDisplayInputPrompt (displayInputPromptEvent);
        }
    }

    public void hideInputPrompt () {
        if (onHideInputPrompt != null) {
            onHideInputPrompt ();
        }
    }
}