using UnityEngine;

public class DisplayInputPromptEvent {

    private GameObject target;
    private string key;
    private string label;
    private Vector2 offset;

    public DisplayInputPromptEvent (GameObject target, string key, string label, Vector2 offset) {
        this.target = target;
        this.key = key;
        this.label = label;
        this.offset = offset;
    }

    public GameObject getTarget () {
        return target;
    }

    public string getKey () {
        return key;
    }

    public string getLabel () {
        return label;
    }

    public Vector2 getOffset () {
        return offset;
    }
}
