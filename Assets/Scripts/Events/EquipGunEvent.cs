using UnityEngine;

public class EquipGunEvent {

    public GameObject character { get; set; }
    public bool isPlayer { get; set; }

    public EquipGunEvent (GameObject character, bool isPlayer) {
        this.character = character;
        this.isPlayer = isPlayer;
    }
}
