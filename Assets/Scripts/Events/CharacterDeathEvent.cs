using UnityEngine;

public class CharacterDeathEvent {

    public GameObject character { get; set; }
    public bool deathByMelee { get; set; }
    public bool isPlayer { get; set; }

    public CharacterDeathEvent (GameObject character, bool deathByMelee, bool isPlayer) {
        this.character = character;
        this.deathByMelee = deathByMelee;
        this.isPlayer = isPlayer;
    }
}
