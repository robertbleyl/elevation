using FMODUnity;
using FMOD.Studio;
using UnityEngine;

public class MusicController : MonoBehaviour {

    [SerializeField]
    private EventReference musicEvent;

    private EventInstance musicEventInstance;

    private bool isFadeMusicIn;

    private float fadeMusicTimer;
    private float fadeMusicTime;

    private void Start () {
        Events.instance.onLevelFinished += onLevelFinsihed;
        Events.instance.onCharacterDied += onCharacterDied;

        playMusic (musicEvent);
    }

    private void OnDestroy () {
        musicEventInstance.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        musicEventInstance.release ();

        Events.instance.onLevelFinished -= onLevelFinsihed;
        Events.instance.onCharacterDied -= onCharacterDied;
    }

    private void onLevelFinsihed () {
        stopMusic (1f);
    }

    private void onCharacterDied (CharacterDeathEvent characterDeathEvent) {
        if (characterDeathEvent.isPlayer) {
            stopMusic (1f);
        }
    }

    private bool isPlaying (EventInstance instance) {
        FMOD.Studio.PLAYBACK_STATE state;
        instance.getPlaybackState (out state);
        return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
    }

    private void Update () {
        if (fadeMusicTimer > 0f) {
            fadeMusicTimer -= Time.deltaTime;

            if (fadeMusicTimer <= 0f && !isFadeMusicIn) {
                musicEventInstance.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                musicEventInstance.release ();
            } else {
                float volume = fadeMusicTimer / fadeMusicTime;

                if (isFadeMusicIn) {
                    volume = 1f - volume;
                }

                musicEventInstance.setVolume (volume);
            }
        }
    }

    public void playMusic (EventReference musicEvent) {
        if (isPlaying (musicEventInstance)) {
            musicEventInstance.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            musicEventInstance.release ();
        }

        musicEventInstance = FMODUnity.RuntimeManager.CreateInstance (musicEvent);
        musicEventInstance.start ();
        musicEventInstance.setVolume (1f);
    }

    public void stopMusic (float stopMusicTime) {
        fadeMusicTime = stopMusicTime;
        fadeMusicTimer = stopMusicTime;
        this.isFadeMusicIn = false;
    }

    public void fadeMusicOut (float fadeMusicTime) {
        this.fadeMusicTime = fadeMusicTime;
        this.fadeMusicTimer = fadeMusicTime;
        this.isFadeMusicIn = false;
    }

    public void fadeMusicIn (float fadeMusicTime) {
        this.fadeMusicTime = fadeMusicTime;
        this.fadeMusicTimer = fadeMusicTime;
        this.isFadeMusicIn = true;
    }
}
